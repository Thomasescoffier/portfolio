<!doctype html>
<html class="no-js" lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>About</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta property="og:image" content="img/thumb.png">
        <meta property="og:image:type" content="image/png">
        <meta property="og:image:width" content="1920">
        <meta property="og:image:height" content="1080">
        <!-- <link rel="manifest" href="site.webmanifest"> -->
        <!-- Place favicon.ico in the root directory -->
        <link rel="stylesheet" href="/css/style.css">
    </head>
    <body>
        <!-- CSS here -->
        <link rel="stylesheet" href="/css/slicknav.css">
        <link rel="stylesheet" href="/css/bootstrap.min.css">
        <link rel="stylesheet" href="/css/font-awesome.min.css">
        <!-- <link rel="stylesheet" href="/css/responsive.css"> -->
        <!--[if lte IE 9]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->
        <!-- header-start -->
        <header>
            <div class="header-area ">
                <div id="sticky-header" class="main-header-area">
                    <div class="container-fluid">
                        <div class="row align-items-center">
                            <div class="col-xl-3 col-lg-2">
                                <div class="logo">
                                    <a href="<?= $language ?>">
                                        <img src="/img/logo.svg" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="col-xl-9 col-lg-10">
                                <div class="main-menu  d-none d-lg-block text-right">
                                    <nav>
                                        <ul id="navigation">
                                            <li><a class="active" href="<?= $language ?>">Portofio</a></li>
                                            <li><a href="<?= $language . "#contact" ?>">Contact</a></li>
                                            <li><a target="_blank" href="img/about/CV 2020 Thomas ESCOFFIER.pdf">My CV</a></li>
                                            <li class="font-awesome">
												<a href="<?= "/" . $otherLangugage . "/about" ?>"><i class="fas fa-flag"></i> <?= strtoupper($otherLangugage) ?></a>
											</li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="mobile_menu d-block d-lg-none"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- header-end -->
        <!-- bradcam_area  -->
        <div class="bradcam_area">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="bradcam_text text-center">
                            <h3>About me</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /bradcam_area  -->
        <div class="about_wrap_area">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-11">
                        <div class="about_heading text-center">
                            <h3>As an art student, I’m looking to specialize in <b>Game Design and Level Design</b>. I work a lot in self-taught and I would like to find new experiences to improve</h3>
                        </div>
                    </div>
                </div>
                <div class="about_info_wrap">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="single_info">
                                <p>I graduated in 2015 and went to medical school. After a year in PACES (first common year of health studies), I decide to take a s <b>year of training in Graphic Design</b> (ESA Games, Carpentras) which allows me to discover a new way in art. I then start a <b>license in Digital Arts</b> (EVMAN) from 2017 to 2020</p>
                                
                                <br><br>
                                <p>After two years in this license, I discover the world of Game Design through a board game project:<b>Vive le Roi</b>. By finding this new profession, I am very interested in it and decide to train<b>in self-taught next to my license</b>. At the end of my second year, I participate in an internship in a video game studio at the Sorbonne: <a target="_blank" href="http://www.ikigai.games/games/gamesList">Ikigai</a>. I learn Game Design and the work of <b>QA Tester</b>.
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="single_info">
                                
                                <p>From the 3rd year, I am fully involved in Game Design and other professions such as <b>Level Design or Team Management</b>. Thus, I work with my first development team, on two games: <b>Emily’s friend and Spirited Debate</b>. In 2020, My goal is to find <b>new experiences in the field of video games</b>and to understand <b>how the work of Game Designer</b> fits into a development team</p>
                                <!-- FAIRE UNE MODIF ENTRE LINKS AND BOLD -->
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-lg-10">
                            <div class="about_banner">
                                <img src="/img/about/about_banner.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- service_area  -->
        <div class="service_area">
            <div class="outline_text white d-none d-lg-block">
                <img src="/img/bg.svg" alt="">
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-6">
                        <div class="single_service">
                            <div class="icon">
                                <img src="/img/svg_icon/1.svg" alt="">
                            </div>
                            <h3>Game & Level Design</h3>
                            <p>Software used: Unity, Blender/Maya, Word/Excel (Game Design), Illustrator (Level Design), Hammer (Level Editor Team Fortress 2)</p>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6">
                        <div class="single_service">
                            <div class="icon">
                                <img src="/img/svg_icon/2.svg" alt="">
                            </div>
                            <h3>Graphic Design</h3>
                            <p>Vector creation allows you to compose <br> a simple, clean and understandable image. <br> I use it for UI design <br> and Level Design.</p>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6">
                        <div class="single_service">
                            <div class="icon">
                                <img src="/img/svg_icon/3.svg" alt="">
                            </div>
                            <h3>Programming</h3>
                            <p> I know how to use HTML, CSS and JS. <br> Since 2019, I practice regularly <br> programming in C#. I want to <br> learn C and C++ using Unreal Engine.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/ service_area  -->
        <div class="creative_people">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section_title text-center">
                            <h4>The creative professionals</h4>
                        </div>
                    </div>
                </div>
                <div class="border_bottom">
                    <div class="row">
                        <div class="col-lg-3 col-md-6">
                            <div class="single_people">
                                <div class="thumb">
                                    <a target="_blank" href="https://www.instagram.com/saltea.png/"><img src="/img/creative_blog/author2.png" alt="">
                                    </div>
                                    <div class="people_info">
                                        <h4>MANON POPIELARZ</h4>
                                        <p>Sound Designer, Photographer, Editor</p></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6">
                                <div class="single_people">
                                    <div class="thumb">
                                        <a target="_blank" href="https://www.artstation.com/steelar_bubbles"><img src="/img/creative_blog/author3.png" alt="">
                                        </div>
                                        <div class="people_info">
                                            <h4>Simon Sigal</h4>
                                            <p>FX Artist, Environnement artist</p></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6">
                                    <div class="single_people">
                                        <div class="thumb">
                                            <a target="_blank" href="https://www.instagram.com/clementmbt_design/">
                                                <img src="/img/creative_blog/author.png" alt="">
                                            </div>
                                            <div class="people_info">
                                                <h4>Clément Maubert</h4>
                                                <p>Artist Modeller and 3D Animator</p></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-6">
                                        <div class="single_people">
                                            <div class="thumb">
                                                <img src="/img/creative_blog/author4.png" alt="">
                                            </div>
                                            <div class="people_info">
                                                <h4>Jacklyn Pusung</h4>
                                                <p>Graphic Designer, Front-End Web Design</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <!-- discuss_projects_start  -->
                    <div class="discuss_projects">
                        <div class="outline_text white project d-none d-lg-block">
                            <img src="/img/bg.svg" alt="">
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-xl-12">
                                    <div class="project_text text-center">
                                        <h3>Contact me to start working together</h3>
                                        <a class="boxed-btn3" href="<?= $language . "#contact" ?>">Contact me</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- discuss_projects_end  -->
                    <!-- footer start -->
                    <footer class="footer">
                        <div class="copy-right_text">
                            <div class="container">
                                <div class="row">
                                    <div class="col-xl-12">
                                        <p class="copy_right text-center">
                                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                            Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </footer>
                    <!--/ footer end  -->
                    <!-- JS here -->
                    <script src="js/vendor/modernizr-3.5.0.min.js"></script>
                    <script src="js/vendor/jquery-1.12.4.min.js"></script>
                    <script src="js/popper.min.js"></script>
                    <script src="js/bootstrap.min.js"></script>
                    <script src="js/owl.carousel.min.js"></script>
                    <script src="js/isotope.pkgd.min.js"></script>
                    <script src="js/ajax-form.js"></script>
                    <script src="js/waypoints.min.js"></script>
                    <script src="js/jquery.counterup.min.js"></script>
                    <script src="js/imagesloaded.pkgd.min.js"></script>
                    <script src="js/scrollIt.js"></script>
                    <script src="js/jquery.scrollUp.min.js"></script>
                    <script src="js/wow.min.js"></script>
                    <script src="js/nice-select.min.js"></script>
                    <script src="js/jquery.slicknav.min.js"></script>
                    <script src="js/jquery.magnific-popup.min.js"></script>
                    <script src="js/plugins.js"></script>
                    <script src="js/gijgo.min.js"></script>
                    <!--contact js-->
                    <script src="js/contact.js"></script>
                    <script src="js/jquery.ajaxchimp.min.js"></script>
                    <script src="js/jquery.form.js"></script>
                    <script src="js/jquery.validate.min.js"></script>
                    <script src="js/mail-script.js"></script>
                    <script src="js/main.js"></script>
                </body>
            </html>