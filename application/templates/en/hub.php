<!doctype html>
<html class="no-js" lang="fr">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title>Thomas ESCOFFIER</title>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta property="og:image" content="img/thumb.png">
		<meta property="og:image:type" content="image/png">
		<meta property="og:image:width" content="1920">
		<meta property="og:image:height" content="1080">
		<!-- <link rel="manifest" href="site.webmanifest"> -->
		<!-- Place favicon.ico in the root directory -->
        <link rel="stylesheet" href="/css/style.css">
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
		<link rel="icon" href="favicon.ico" type="image/x-icon">
    </head>
    <body>
        <!-- CSS here -->
        <link rel="stylesheet" href="/css/slicknav.css">
        <link rel="stylesheet" href="/css/bootstrap.min.css">
        <link rel="stylesheet" href="/css/font-awesome.min.css">
		
		<!-- <link rel="stylesheet" href="/css/responsive.css"> -->
		<!--[if lte IE 9]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
		<![endif]-->
		<!-- header-start -->
		<header>
			<div class="header-area ">
				<div id="sticky-header" class="main-header-area">
					<div class="container-fluid">
						<div class="row align-items-center">
							<div class="col-xl-3 col-lg-2">
								<div class="logo">
									<a href="<?= $language ?>">
										<img src="/img/logo.svg" alt="">
									</a>
								</div>
							</div>
							<div class="col-xl-9 col-lg-10">
								<div class="main-menu  d-none d-lg-block text-right">
									<nav>
										<ul id="navigation">
											<li  onclick="scrollDown('#contact')">Contact</li>
											<li><a href="<?= $language . "/about" ?>">About</a></li>
											<li><a target="_blank" href="img/about/CV 2020 Thomas ESCOFFIER.pdf">My CV</a></li>
											<li class="font-awesome">
												<a href="<?= "/" . $otherLangugage ?>"><i class="fas fa-flag"></i> <?= strtoupper($otherLangugage) ?></a>
											</li>
										</ul>
									</nav>
								</div>
							</div>
							<div class="col-12">
								<div class="mobile_menu d-block d-lg-none"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>
		<!-- header-end -->
		<!-- slider_area_start -->
		<div class="slider_area">
			<div class="single_slider  d-flex align-items-center slider_bg_1">
				<div class="outline_text d-none d-lg-block">
					<img src="/img/bg.svg" alt="">
				</div>
				<div class="container">
					<div class="row align-items-center justify-content-center">
						<div class="col-xl-12">
							<div class="slider_text text-center">
								<div id="welcome" class="welcomeText">
									<span>Welcome</span>
									<h3>
									Large company or small startup, from design to production, I want to be part of your team!
									</h3>
								</div>
								<img onclick="scrollDown('#projects')" src="/img/down-arrow.svg">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- slider_area_end -->
		<div class="portfolio_area blue" id="projects">
			<!-- <div class="container-fluid p-0"> -->
			<div class="portfolio_wrap">
				<a href="<?= $language . "/projects/emily" ?>">
					<div class=" single_gallery">
						
						<div class="thumb">
							<img src="/img/portfolio/1.svg" alt="">
						</div>
						<div class="gallery_hover">
							<div class="hover_inner">
								<span>University Project done with Unity 3D</span>
								<h3>Emily's Friend</h3>
								
							</div>
						</div>
					</div>
				</a>
				
				<a href="<?= $language . "/projects/you-manity" ?>">
					<div class="single_gallery">
						
						<div class="thumb">
							<img src="/img/portfolio/2.svg" alt="">
						</div>
						<div class="gallery_hover">
							<div class="hover_inner">
								<span>University Project done with Unity 3D</span>
								<h3>You-Manity</h3>
							</div>
						</div>
					</div>
				</a>
				<a href="<?= $language . "/projects/vive-le-roi" ?>">
					<div class="single_gallery">
						<div class="thumb">
							<img src="/img/portfolio/3.svg" alt="">
						</div>
						<div class="gallery_hover">
							<div class="hover_inner">
								<span>University Project : Board Game</span>
								<h3>Vive le Roi !</h3>
							</div>
						</div>
					</div>
				</a>
			</div>
			<div class="portfolio_wrap orange">
				<a href="<?= $language . "/projects/dark-school" ?>">
					<div class=" single_gallery">
						<div class="thumb">
							<img src="/img/portfolio/4.svg" alt="">
						</div>
						<div class="gallery_hover">
							<div class="hover_inner">
								<span>University Project done with Max 7 Cycling </span>
								<h3>Dark School <br> (Installation)</h3>
							</div>
						</div>
					</div>
				</a>
				<a href="<?= $language . "/projects/organisation" ?>">
					<div class=" single_gallery">
						<div class="thumb">
							<img src="/img/portfolio/11.svg" alt="">
						</div>
						<div class="gallery_hover">
							<div class="hover_inner">
								<span>Project organisation (Excel/GitHub)</span>
								<h3>Team management</h3>
							</div>
						</div>
					</div>
				</a>
				<a href="<?= $language . "/projects/internship-hexaverse" ?>">
					<div class="single_gallery">
						
						<div class="thumb">
							<img src="/img/portfolio/5.svg" alt="">
						</div>
						<div class="gallery_hover">
							<div class="hover_inner">
								<span>Internship experience : Game Design</span>
								<h3>Hexaverse</h3>
							</div>
						</div>
					</div>
				</a>
			</div>
			<div class="portfolio_wrap blue">
				<a href="<?= $language . "/projects/graphic-design" ?>">
					<div class="single_gallery">
						<div class="thumb">
							<img src="/img/portfolio/7.svg" alt="">
						</div>
						<div class="gallery_hover">
							<div class="hover_inner">
								<span>Personal projects on Illustrator</span>
								<h3>Graphic Design</h3>
							</div>
						</div>
					</div>
				</a>
				<a href="<?= $language . "/projects/dungeon-and-dragon" ?>">
					<div class=" single_gallery">
						<div class="thumb">
							<img src="/img/portfolio/8.svg" alt="">
						</div>
						<div class="gallery_hover">
							<div class="hover_inner">
								<span>Personal projects : Level Design </span>
								<h3>Dungeon and Dragon</h3>
							</div>
						</div>
					</div>
				</a>
				<a href="<?= $language . "/projects/gamejams" ?>">
					<div class=" single_gallery">
						<div class="thumb">
							<img src="/img/portfolio/9.svg" alt="">
						</div>
						<div class="gallery_hover">
							<div class="hover_inner">
								<span>Personal projects : Game Design</span>
								<h3>Game Jams</h3>
							</div>
						</div>
					</div>
				</a>
			</div>
			<!-- </div> -->
			<div class="more_works text-center">
				<a href="<?= $language . "/projects" ?>">More projects</a>
			</div>
		</div>
		<!-- service_area  -->
		<div class="service_area">
			<div class="outline_text white d-none d-lg-block">
				<img src="/img/bg.svg" alt="">
			</div>
			<div class="container">
				<div class="row">
					<div class="col-xl-4 col-lg-4 col-md-6">
						<div class="single_service">
							<div class="icon-info">
								<img src="/img/svg_icon/1.svg" alt="">
							</div>
							<h3>Game & Level Design</h3>
							<p>I look for a <b>unique experience </b><br> before the script or graphic style. <br><b>The way a game is played</b>, defined, <br> everything that composes it.</p>
						</div>
					</div>
					<div class="col-xl-4 col-lg-4 col-md-6">
						<div class="single_service">
							<div class="icon-info">
								<img src="/img/svg_icon/2.svg" alt="">
							</div>
							<h3>Graphic Design</h3>
							<p>Vector creation allows to compose <br> <b>a simple, clean and understandable image.</b> <br> I use it for <b>UI design</b> <br> and for <b>Level Design.</b></p>
						</div>
					</div>
					<div class="col-xl-4 col-lg-4 col-md-6">
						<div class="single_service">
							<div class="icon-info">
								<img src="/img/svg_icon/3.svg" alt="">
							</div>
							<h3>Programming</h3>
							<p>I know how to use <b>HTML, CSS and JS.</b> <br> Since 2019, I practice regularly <br> <b>programming in C#</b>. I wish to learn <b> C and C++ with the use of Unreal Engine</b></p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/ service_area  -->
		
		<!-- discuss_projects_start  -->
		<div id="contact" class="discuss_projects">
			<div class="outline_text white project d-none d-lg-block">
				<img src="/img/bg.svg" alt="">
			</div>
			<div class="container">
				<div class="row">
					<div class="col-xl-12">
						<div class="project_text text-center">
							<h3>Contact me to start working together</h3>
							<section class="section_padding">
								<div class="media contact-info">
									<div class="media-body">
										<h2><a href="mailto:a.thomasescoffier@gmail.com">a.thomas.escoffier@gmail.com</a></h2>
										<h3>You can email me all your questions.</h3>
									</div>
								</div>
								<div class="media contact-info">
									<div class="media-body">
										<h2><a href="tel:+33.6.13.17.77.83">+33.6.13.17.77.83</a></h2>
										<h3>Monday to Sunday from 8am to 9am and from 4pm to 7pm</h3>
									</div>
								</div>
								<div class="media contact-info">
									<div class="media-body ">
										<h2 class="nolink">Paris region, France.</h2>
										<h3>Champ-sur-Marne, 77420</h3>
									</div>
								</div>
								<div class="media contact-info">
									<div class="media-body">
										<h2><a target="_blank" href="img/about/CV 2020 Thomas ESCOFFIER.pdf">My Curriculum Vitae in PDF</a></h2>
										<h3>Available in printable version</h3>
									</div>
								</div>
							</section>
							<div class="up-arrow">
							<img  onclick="scrollDown('#welcome')" src="/img/up-arrow.svg">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- discuss_projects_end  -->
		<!-- footer start -->
		<footer class="footer">
			<div class="copy-right_text">
				<div class="container">
					<div class="row">
						<div class="col-xl-12">
							<p class="copy_right text-center">
								<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
								Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
								<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
							</p>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<!--/ footer end  -->
		<!-- JS here -->
		<script src="js/vendor/modernizr-3.5.0.min.js"></script>
		<script src="js/vendor/jquery-1.12.4.min.js"></script>
		<script src="js/popper.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/owl.carousel.min.js"></script>
		<script src="js/isotope.pkgd.min.js"></script>
		<script src="js/ajax-form.js"></script>
		<script src="js/waypoints.min.js"></script>
		<script src="js/jquery.counterup.min.js"></script>
		<script src="js/imagesloaded.pkgd.min.js"></script>
		<script src="js/scrollIt.js"></script>
		<script src="js/jquery.scrollUp.min.js"></script>
		<script src="js/wow.min.js"></script>
		<script src="js/nice-select.min.js"></script>
		<script src="js/jquery.slicknav.min.js"></script>
		<script src="js/jquery.magnific-popup.min.js"></script>
		<script src="js/plugins.js"></script>
		<script src="js/gijgo.min.js"></script>
		<!--contact js-->
		<script src="js/contact.js"></script>
		<script src="js/jquery.ajaxchimp.min.js"></script>
		<script src="js/jquery.form.js"></script>
		<script src="js/jquery.validate.min.js"></script>
		<script src="js/mail-script.js"></script>
		<script src="js/main.js"></script>
	</body>
</html>