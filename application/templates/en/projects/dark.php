<!doctype html>
<html class="no-js" lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Dark School</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta property="og:image" content="img/thumb.png">
        <meta property="og:image:type" content="image/png">
        <meta property="og:image:width" content="1920">
        <meta property="og:image:height" content="1080">
        <!-- <link rel="manifest" href="site.webmanifest"> -->
        <!-- Place favicon.ico in the root directory -->
        <link rel="stylesheet" href="/css/style.css">
    </head>
    <body>
        <!-- CSS here -->
        <link rel="stylesheet" href="/css/slicknav.css">
        <link rel="stylesheet" href="/css/bootstrap.min.css">
        <link rel="stylesheet" href="/css/font-awesome.min.css">
        
        <!-- <link rel="stylesheet" href="/css/responsive.css"> -->
        <!--[if lte IE 9]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->
        <!-- header-start -->
        <header>
            <div class="header-area ">
                <div id="sticky-header" class="main-header-area">
                    <div class="container-fluid">
                        <div class="row align-items-center">
                            <div class="col-xl-3 col-lg-2">
                                <div class="logo">
                                    <a href="<?= $language ?>">
                                        <img src="/img/logo.svg" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="col-xl-9 col-lg-10">
                                <div class="main-menu  d-none d-lg-block text-right">
                                    <nav>
                                        <ul id="navigation">
                                            <li><a class="active" href="<?= $language ?>">Portofio</a></li>
                                            <li><a href="<?= $language . "#contact" ?>">Contact</a></li>
                                            <li><a href="<?= $language . "/about" ?>">About</a></li>
                                            <li class="font-awesome">
												<a href="<?= "/" . $otherLangugage . "/projects/dark-school"?>"><i class="fas fa-flag"></i> <?= strtoupper($otherLangugage) ?></a>
											</li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="mobile_menu d-block d-lg-none"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- header-end -->
        <!-- bradcam_area  -->
        <div class="bradcam_area">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="bradcam_text text-center">
                            <h3>Dark School</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /bradcam_area  -->
        <div class="work_details_area">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-10">
                        <div class="details_catagory_wrap">
                            <div class="single_catagory">
                                <span>Client</span>
                                <h4>UPEM / Gustave Eiffel</h4>
                            </div>
                            <div class="single_catagory">
                                <span>Category</span>
                                <h4>Game with choices - horror</h4>
                            </div>
                            <div class="single_catagory">
                                <span>Date of the project</span>
                                <h4>January 2019 - March 2019</h4>
                            </div>
                            <div class="single_catagory">
                                <a class="boxed-btn3 porject_btn" target="_blank" href="http://projets-arts-numeriques.eklablog.com/dark-schools-a161981846">Link to the Project</a>
                            </div>
                        </div>
                        <div class="banner">
                            <img src="/img/details/Dark_details.PNG" alt="">
                        </div>
                        <div class="details_info">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="info">
                                        <p>For Dante, Hell is composed of <b>nine circular shapes</b> which superimpose on each other, competing in terms of punishment until <b>the ninth and last circle where Lucifer</b>, the prince of darkness, sits. For a UPEM student in the middle of a semester rendezvous, Hell is this imposing <b>building that spreads</b> over three floors made up of classrooms. A hell where you have to struggle to keep your eyes open, preserve your motivation and <b>never let yourself be discouraged </b> to finally arrive at the summer holidays you so desire. This hell is nine months of hard work where the <b>days follow and resemble eachother</b> and where the slightest escape is good to take.
                                            <br><br>
                                            <b>Dark Schools</b>, is a somewhat supernatural glimpse of our personal vision of Hell. It is <b>the daily routine of a student</b> struggling for his physical survival but above all <b>psychological</b> and, where every choice is decisive. Inspired by the almost eponymous <b>RPG</b> (role playing game) <b>Dark Souls</b>, this FMV (full motion video) consists of <b>filmed sequences</b> and "action" sequences called  <b>QTE</b> (quick time event)  rushes the spectator into the shoes of an actor and pushes him to act and interact to save his own skin and sometimes even that of his friends…
                                        </p>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="info">
                                        <p>In a <b>dark and anguishing atmosphere</b>, Dark Schools proposes to transpose the spectator <b>into the shoes of this famous student</b> and to make him spend a day at the university in what proves to be a real descent to the Underworld. Through <b>sensors</b>, it will be necessary to advance and overcome the many difficulties, all within <b>a time span that is counted</b>. The use of these sensors is indicated by<b>small icons</b> that appear on the screen at the right time.
                                            <br><br>
                                            The user has only to use <b>reflexes and speed</b> to get out of the many situations in which he finds himself immersed.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="details_single_img">
                            <div class="row">
                                <div class="col-lg-6 col-md-6">
                                    <div class="thumb">
                                        <img src="/img/details/8.jpg" alt="">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="thumb">
                                        <img src="/img/details/7.jpg" alt="">
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12">
                                    <div class="thumb d-flex justify-content-center">
                                        <iframe width="1280" height="720" src="https://www.youtube-nocookie.com/embed/zxGmTB0TR1c" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!--/ service_area  -->
        <div class="creative_people">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section_title text-center">
                            <h4>Professionals on Dark School</h4>
                        </div>
                    </div>
                </div>
                <div class="border_bottom">
                    <div class="row">
                        <div class="col-lg-3 col-md-6">
                            <div class="single_people">
                                <div class="thumb">
                                    <a target="_blank" href="https://www.instagram.com/saltea.png/"><img src="/img/creative_blog/author2.png" alt="">
                                    </div>
                                    <div class="people_info">
                                        <h4>Manon POPIELARZ</h4>
                                        <p>Sound Designer, Photographer, Editor</p></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6">
                                <div class="single_people">
                                    <div class="thumb">
                                        <a target="_blank" href="https://www.artstation.com/steelar_bubbles">
                                            <img src="/img/creative_blog/author3.png" alt="">
                                        </div>
                                        <div class="people_info">
                                            <h4>Simon SIGAL</h4>
                                            <p>FX Artist, Environnement artist</p></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6">
                                    <div class="single_people">
                                        <div class="thumb">
                                            <a target="_blank" href="https://www.linkedin.com/in/agathe-bosch?fbclid=IwAR0koP27g0Ek_NiPLN5pWQk4T0oRzM30PPhNP688KVLQbdNa39N6vkTN4S8">
                                                <img src="/img/creative_blog/agathe.jpg" alt="">
                                            </div>
                                            <div class="people_info">
                                                <h4>Agathe BOSCH COZZOLINO</h4>
                                                <p>Infographist, Digital Artist and Web Designer</p></a>
                                                <a target="_blank" href="https://www.artstation.com/caligula_art?fbclid=IwAR14zst_OFtZnFQmDGHMbux_8-LkK9QwTvOZBq70H7JlfpDEv2sdbZwPRUo"><p><b>Artstation</b></p></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-6">
                                        <div class="single_people">
                                            <div class="thumb">
                                                <a target="_blank" href="https://fr.linkedin.com/in/thomas-escoffier-a4aa2917a">
                                                    <img src="/img/creative_blog/author5.png" alt="">
                                                </div>
                                                <div class="people_info">
                                                    <h4>Thomas Escoffier</h4>
                                                    <p>Game Designer, Level Designer and Programmer</p></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- discuss_projects_start  -->
                        <div class="discuss_projects">
                            <div class="outline_text white project d-none d-lg-block">
                                <img src="/img/bg.svg" alt="">
                            </div>
                            <div class="container">
                                <div class="row">
                                    <div class="col-xl-12">
                                        <div class="project_text text-center">
                                            <h3>Contact me to start working together.</h3>
                                            <a class="boxed-btn3" href="<?= $language . "#contact" ?>">Contact me</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- discuss_projects_end  -->
                        <!-- footer start -->
                        <footer class="footer">
                            <div class="copy-right_text">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-xl-12">
                                            <p class="copy_right text-center">
                                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                                Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </footer>
                        <!--/ footer end  -->
                        <!-- JS here -->
                        <script src="js/vendor/modernizr-3.5.0.min.js"></script>
                        <script src="js/vendor/jquery-1.12.4.min.js"></script>
                        <script src="js/popper.min.js"></script>
                        <script src="js/bootstrap.min.js"></script>
                        <script src="js/owl.carousel.min.js"></script>
                        <script src="js/isotope.pkgd.min.js"></script>
                        <script src="js/ajax-form.js"></script>
                        <script src="js/waypoints.min.js"></script>
                        <script src="js/jquery.counterup.min.js"></script>
                        <script src="js/imagesloaded.pkgd.min.js"></script>
                        <script src="js/scrollIt.js"></script>
                        <script src="js/jquery.scrollUp.min.js"></script>
                        <script src="js/wow.min.js"></script>
                        <script src="js/nice-select.min.js"></script>
                        <script src="js/jquery.slicknav.min.js"></script>
                        <script src="js/jquery.magnific-popup.min.js"></script>
                        <script src="js/plugins.js"></script>
                        <script src="js/gijgo.min.js"></script>
                        <!--contact js-->
                        <script src="js/contact.js"></script>
                        <script src="js/jquery.ajaxchimp.min.js"></script>
                        <script src="js/jquery.form.js"></script>
                        <script src="js/jquery.validate.min.js"></script>
                        <script src="js/mail-script.js"></script>
                        <script src="js/main.js"></script>
                    </body>
                </html>