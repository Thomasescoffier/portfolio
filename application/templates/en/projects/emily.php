<!doctype html>
<html class="no-js" lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Emily's Friend</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta property="og:image" content="img/thumb.png">
        <meta property="og:image:type" content="image/png">
        <meta property="og:image:width" content="1920">
        <meta property="og:image:height" content="1080">
        <!-- <link rel="manifest" href="site.webmanifest"> -->
        <!-- Place favicon.ico in the root directory -->
        <link rel="stylesheet" href="/css/style.css">
    </head>
    <body>
        <!-- CSS here -->
        <link rel="stylesheet" href="/css/slicknav.css">
        <link rel="stylesheet" href="/css/bootstrap.min.css">
        <link rel="stylesheet" href="/css/font-awesome.min.css">
        <!-- <link rel="stylesheet" href="/css/responsive.css"> -->
        <!--[if lte IE 9]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->
        <!-- header-start -->
        <header>
            <div class="header-area ">
                <div id="sticky-header" class="main-header-area">
                    <div class="container-fluid">
                        <div class="row align-items-center">
                            <div class="col-xl-3 col-lg-2">
                                <div class="logo">
                                    <a href="<?= $language ?>">
                                        <img src="/img/logo.svg" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="col-xl-9 col-lg-10">
                                <div class="main-menu  d-none d-lg-block text-right">
                                    <nav>
                                        <ul id="navigation">
                                            <li><a class="active" href="<?= $language ?>">Portofio</a></li>
                                            <li><a href="<?= $language . "#contact" ?>">Contact</a></li>
                                            <li><a href="<?= $language . "/about" ?>">About</a></li>
                                            <li class="font-awesome">
												<a href="<?= "/" . $otherLangugage . "/projects/emily" ?>"><i class="fas fa-flag"></i> <?= strtoupper($otherLangugage) ?></a>
											</li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="mobile_menu d-block d-lg-none"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- header-end -->
        <!-- bradcam_area  -->
        <div class="bradcam_area">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="bradcam_text text-center">
                            <h3>Emily's Friend</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /bradcam_area  -->
        <div class="work_details_area">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-10">
                        <div class="details_catagory_wrap">
                            <div class="single_catagory">
                                <span>Client</span>
                                <h4>UPEM / Gustave Eiffel</h4>
                            </div>
                            <div class="single_catagory">
                                <span>Category</span>
                                <h4>PC game - Adventure</h4>
                            </div>
                            <div class="single_catagory">
                                <span>Date of the project</span>
                                <h4>September 2019 - April 2020</h4>
                            </div>
                            <div class="single_catagory">
                                <span>Download</span>
                                <div class="social_links">
                                    <a href="#"> <h4>Itch.io</h4> </a>
                                </div>
                            </div>
                            <div class="single_catagory">
                                <a class="boxed-btn3 porject_btn" target="_blank" href="https://greenpeas.trashmates.fr/">Link to the Project</a>
                            </div>
                        </div>
                        <div class="banner">
                            <img src="/img/details/Emily_details.png" alt="">
                        </div>
                        <div class="details_info">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="info">
                                        <p>Emily is a <b>9 ½ year old girl</b> living with her father John. One day, while Emily is playing in her garden, she hears a little noise in the garbage next to the house. She then discovers <b>a small squirrel</b> digging through the trash in search of food. The girl hastens to give him acorns littered on the ground, which the little animal accepts.

                                        <br><br>As Emily makes a new friend, <b>John goes out into the garden </b>to surprise Emily with <b>a wild animal</b> on her shoulder. The squirrel sees the father arrives, flees into the tree planted in the garden and hides itself from the sight of all. Lovingly giving it the name<b> of Tofu</b>, Emily will have to help her new friend find food because <b>winter is coming</b>. However, Tofu’s presence must remain <b>secret in the eyes of her father.</b></p>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="info">
                                        <p>Emily’s Friend is a student PC game project that is played <b> with a keyboard or joystick</b>, created by a team of <b>five people</b>. Accompany Emily on a dangerous journey <b>in search of food for Tofu</b>The young girl will have to remain brave in the face of <b>visual pollutions </b> everywhere on her path.
                                        
                                        <br><br> You must protect Emily during her adventure by avoiding her being <b>affected by her environment and falling into tears on the ground...</b> Fortunately, the <b>nature is there to appease her.</b> </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="details_single_img">
                            <div class="row">
                                <div class="col-lg-6 col-md-6">
                                    <video width="100%" height="100%" controls>
                                        <source src="/img/details/Trailer.mp4" type="video/mp4">
                                    </video>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="thumb">
                                        <img src="/img/details/1.png" alt="">
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/ service_area  -->
        <div class="creative_people">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section_title text-center">
                            <h4>Professionals on Emily's Friend</h4>
                        </div>
                    </div>
                </div>
                <div class="border_bottom">
                    <div class="row">
                        <div class="col-lg-3 col-md-6">
                            <div class="single_people">
                                <div class="thumb">
                                    <a target="_blank" href="https://www.instagram.com/saltea.png/"><img src="/img/creative_blog/author2.png" alt="">
                                </div>
                                <div class="people_info">
                                    <h4>Manon Popielarz</h4>
                                    <p>Sound Designer, Photographer, Editor</p></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="single_people">
                                <div class="thumb">
                                    <a target="_blank" href="https://www.artstation.com/steelar_bubbles"><img src="/img/creative_blog/author3.png" alt="">
                                </div>
                                <div class="people_info">
                                    <h4>Simon Sigal</h4>
                                    <p>FX Artist, Environnement artist</p></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="single_people">
                                <div class="thumb">
                                    <a target="_blank" href="https://www.instagram.com/clementmbt_design/">
                                    <img src="/img/creative_blog/author.png" alt="">
                                </div>
                                <div class="people_info">
                                    <h4>Clément Maubert</h4>
                                    <p>Artist Modeller and 3D Animator</p></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="single_people">
                                <div class="thumb">
                                    <img src="/img/creative_blog/author4.png" alt="">
                                </div>
                                <div class="people_info">
                                    <h4>Jacklyn Pusung</h4>
                                    <p>Graphic Designer, Front-End Web Design</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="single_thomas">
                                <div class="thumbnail">
                                    <a target="_blank" href="https://fr.linkedin.com/in/thomas-escoffier-a4aa2917a">
                                    <img src="/img/creative_blog/author5.png" alt="">
                                </div>
                                <div class="people_info">
                                    <h4>Thomas Escoffier</h4>
                                    <p>Game Designer, Level Designer and Programmer</p></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

                    <!-- discuss_projects_start  -->
                    <div class="discuss_projects">
                        <div class="outline_text white project d-none d-lg-block">
                            <img src="/img/bg.svg" alt="">
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-xl-12">
                                    <div class="project_text text-center">
                                        <h3>Contact me to start working together.</h3>
                                        <a class="boxed-btn3" href="<?= $language . "#contact" ?>">Contact me</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- discuss_projects_end  -->
                    <!-- footer start -->
                    <footer class="footer">
                        <div class="copy-right_text">
                            <div class="container">
                                <div class="row">
                                    <div class="col-xl-12">
                                        <p class="copy_right text-center">
                                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                            Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </footer>
                    <!--/ footer end  -->
                    <!-- JS here -->
                    <script src="js/vendor/modernizr-3.5.0.min.js"></script>
                    <script src="js/vendor/jquery-1.12.4.min.js"></script>
                    <script src="js/popper.min.js"></script>
                    <script src="js/bootstrap.min.js"></script>
                    <script src="js/owl.carousel.min.js"></script>
                    <script src="js/isotope.pkgd.min.js"></script>
                    <script src="js/ajax-form.js"></script>
                    <script src="js/waypoints.min.js"></script>
                    <script src="js/jquery.counterup.min.js"></script>
                    <script src="js/imagesloaded.pkgd.min.js"></script>
                    <script src="js/scrollIt.js"></script>
                    <script src="js/jquery.scrollUp.min.js"></script>
                    <script src="js/wow.min.js"></script>
                    <script src="js/nice-select.min.js"></script>
                    <script src="js/jquery.slicknav.min.js"></script>
                    <script src="js/jquery.magnific-popup.min.js"></script>
                    <script src="js/plugins.js"></script>
                    <script src="js/gijgo.min.js"></script>
                    <!--contact js-->
                    <script src="js/contact.js"></script>
                    <script src="js/jquery.ajaxchimp.min.js"></script>
                    <script src="js/jquery.form.js"></script>
                    <script src="js/jquery.validate.min.js"></script>
                    <script src="js/mail-script.js"></script>
                    <script src="js/main.js"></script>
                </body>
            </html>