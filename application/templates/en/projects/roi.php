<!doctype html>
<html class="no-js" lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Long live the King</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta property="og:image" content="img/thumb.png">
        <meta property="og:image:type" content="image/png">
        <meta property="og:image:width" content="1920">
        <meta property="og:image:height" content="1080">
        <!-- <link rel="manifest" href="site.webmanifest"> -->
        <!-- Place favicon.ico in the root directory -->
        <link rel="stylesheet" href="/css/style.css">
    </head>
    <body>
        <!-- CSS here -->
        <link rel="stylesheet" href="/css/slicknav.css">
        <link rel="stylesheet" href="/css/bootstrap.min.css">
        <link rel="stylesheet" href="/css/font-awesome.min.css">
        <!-- <link rel="stylesheet" href="/css/responsive.css"> -->
        <!--[if lte IE 9]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->
        <!-- header-start -->
        <header>
            <div class="header-area ">
                <div id="sticky-header" class="main-header-area">
                    <div class="container-fluid">
                        <div class="row align-items-center">
                            <div class="col-xl-3 col-lg-2">
                                <div class="logo">
                                    <a href="<?= $language ?>">
                                        <img src="/img/logo.svg" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="col-xl-9 col-lg-10">
                                <div class="main-menu  d-none d-lg-block text-right">
                                    <nav>
                                        <ul id="navigation">
                                            <li><a class="active" href="<?= $language ?>">Portofio</a></li>
                                            <li><a href="<?= $language . "#contact" ?>">Contact</a></li>
                                            <li><a href="<?= $language . "/about" ?>">About</a></li>
                                            <li class="font-awesome">
												<a href="<?= "/" . $otherLangugage . "/projects/vive-le-roi" ?>"><i class="fas fa-flag"></i> <?= strtoupper($otherLangugage) ?></a>
											</li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="mobile_menu d-block d-lg-none"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- header-end -->
        <!-- bradcam_area  -->
        <div class="bradcam_area">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="bradcam_text text-center">
                            <h3>Long live the King!</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /bradcam_area  -->
        <div class="work_details_area">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-10">
                        <div class="details_catagory_wrap">
                            <div class="single_catagory">
                                <span>Client</span>
                                <h4>UPEM / Gustave Eiffel</h4>
                            </div>
                            <div class="single_catagory">
                                <span>Category</span>
                                <h4>Card Game - Psychology</h4>
                            </div>
                            <div class="single_catagory">
                                <span>Date of the project</span>
                                <h4> 2017 - 2018</h4>
                            </div>
                            <div class="single_catagory">
                                <span>Progress</span>
                                <div class="social_links">
                                    <h4>Beta</h4>
                                </div>
                            </div>
                        </div>
                        <div class="banner">
                            <img src="/img/details/Roi_details.png" alt="">
                        </div>
                        <div class="details_info">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="info">
                                        <p>You are a citizen of the capital of Hammerfall, city <b>in full political overthrow</b>. The King having seen the revolution of his people burst into the streets, decides to <b>flee and hide among his fellow citizens</b>. With the civil war then raging, the only fear of the<b>revolutionaries is to see the King return to the throne</b>, they will do everything in their power to<b>find him and dispose of him</b>.  The Royalists wait only for the return of their privileges and will try to subdue the revolution to <b>get their sovereign back in his castle</b>.
                                        <br><br>Only, who knows if your neighbor is not a <b>Revolutionary scum or a spoiled Royalist</b>.  It’s up to you to discover it and don’t forget, keep up appearances and <br><b>LONG LIVE THE KING!</b></p>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="info">
                                        <p>Long live the King is a game that builds <b>an environment of conflict and discord</b> where players develop their <b>trust or mistrust</b> towards their playmates. The aim of the gameplay is to create <b>conditions conducive to discussion</b> to invite players to interact with each other.
                                            <br><br> The game has a discrete gameplay to facilitate relationship building between players. The objective of Long live the King is to maintain the interest of the game <b>even for the players eliminated, with a role system</b>.  On the other hand, <b>the game has a weapons system with different distances</b> allowing to attack some players that are in range.
                                        <br><br> Finally, the revolution raging, <b>a "Suspicion Token"</b> circulates among the players. <b>Don’t draw too much attention to yourself</b>, you may be discovered by the most aggressive of the Revolutionaries.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="details_single_img">
                            <div class="row">
                                <div class="col-lg-4 col-md-4">
                                    <div class="thumb">
                                        <img src="/img/details/3.png" alt="">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="thumb">
                                        <img src="/img/details/4.png" alt="">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="thumb">
                                        <img src="/img/details/5.png" alt="">
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12">
                                    <div class="thumb">
                                        <img src="/img/details/6.png" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- discuss_projects_start  -->
        <div class="discuss_projects">
            <div class="outline_text white project d-none d-lg-block">
                <img src="/img/bg.svg" alt="">
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="project_text text-center">
                            <h3>Contact me to start working together.</h3>
                            <a class="boxed-btn3" href="<?= $language . "#contact" ?>">Contact me</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- discuss_projects_end  -->
        <!-- footer start -->
        <footer class="footer">
            <div class="copy-right_text">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <p class="copy_right text-center">
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!--/ footer end  -->
        <!-- JS here -->
        <script src="js/vendor/modernizr-3.5.0.min.js"></script>
        <script src="js/vendor/jquery-1.12.4.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/isotope.pkgd.min.js"></script>
        <script src="js/ajax-form.js"></script>
        <script src="js/waypoints.min.js"></script>
        <script src="js/jquery.counterup.min.js"></script>
        <script src="js/imagesloaded.pkgd.min.js"></script>
        <script src="js/scrollIt.js"></script>
        <script src="js/jquery.scrollUp.min.js"></script>
        <script src="js/wow.min.js"></script>
        <script src="js/nice-select.min.js"></script>
        <script src="js/jquery.slicknav.min.js"></script>
        <script src="js/jquery.magnific-popup.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/gijgo.min.js"></script>
        <!--contact js-->
        <script src="js/contact.js"></script>
        <script src="js/jquery.ajaxchimp.min.js"></script>
        <script src="js/jquery.form.js"></script>
        <script src="js/jquery.validate.min.js"></script>
        <script src="js/mail-script.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>