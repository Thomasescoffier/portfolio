<!doctype html>
<html class="no-js" lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Organisation Détails</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta property="og:image" content="img/thumb.png">
        <meta property="og:image:type" content="image/png">
        <meta property="og:image:width" content="1920">
        <meta property="og:image:height" content="1080">
        <!-- <link rel="manifest" href="site.webmanifest"> -->
        <!-- Place favicon.ico in the root directory -->
        <link rel="stylesheet" href="/css/style.css">
    </head>
    <body>
        <!-- CSS here -->
        <link rel="stylesheet" href="/css/slicknav.css">
        <link rel="stylesheet" href="/css/bootstrap.min.css">
        <link rel="stylesheet" href="/css/font-awesome.min.css">
        <!-- <link rel="stylesheet" href="/css/responsive.css"> -->
        <!--[if lte IE 9]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->
        <!-- header-start -->
        <header>
            <div class="header-area ">
                <div id="sticky-header" class="main-header-area">
                    <div class="container-fluid">
                        <div class="row align-items-center">
                            <div class="col-xl-3 col-lg-2">
                                <div class="logo">
                                    <a href="<?= $language ?>">
                                        <img src="/img/logo.svg" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="col-xl-9 col-lg-10">
                                <div class="main-menu  d-none d-lg-block text-right">
                                    <nav>
                                        <ul id="navigation">
                                            <li><a class="active" href="<?= $language ?>">Portofio</a></li>
                                            <li><a href="<?= $language . "#contact" ?>">Contact</a></li>
                                            <li><a href="<?= $language . "/about" ?>">A propos</a></li>
                                            <li class="font-awesome">
												<a href="<?= "/" . $otherLangugage . "/projects/organisation" ?>"><i class="fas fa-flag"></i> <?= strtoupper($otherLangugage) ?></a>
											</li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="mobile_menu d-block d-lg-none"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- header-end -->
        <!-- bradcam_area  -->
        <div class="bradcam_area">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="bradcam_text text-center">
                            <h3>Gestion d'équipe</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /bradcam_area  -->
        <div class="work_details_area">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-10">
                        <div class="details_catagory_wrap">
                            <div class="single_catagory">
                                <span>Client</span>
                                <h4>Gustave Eiffel / Green Peas</h4>
                            </div>
                            <div class="single_catagory">
                                <span>Categorie</span>
                                <h4>Organisation / Gestion</h4>
                            </div>
                            <div class="single_catagory">
                                <span>Date des projet</span>
                                <h4>2019 - 2020</h4>
                            </div>
                        </div>
                        <div class="banner">
                            <img src="/img/details/Org_details.PNG" alt="">
                        </div>
                        <div class="details_info">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="info">
                                        <p>Depuis le début de la <b>3ème année de ma licence</b>, il nous a été demandé de construire des projets de jeu vidéo, <b>de leur conception à leur réalisation</b>. Cela m'a permis d'expérimenter et de découvrir la <b>gestion d'équipe et la construction de systèmes de travail</b>.
                                    <br><br>L'utilisation <b>d'Excel et de Google Drive</b> est devenue une nécessité pour rester à jour sur l'avancée d'un projet et des étapes à venir. Ainsi, j'ai construit un document composé de plusieurs sections permettant à l'équipe de<b> garder les objectifs clairs</b> sur plusieurs mois. cela s'est avéré une nécessité pour maintenir <b>un débit de travail constant</b> sur plus de 4 projets</p>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="info">
                                    <p>
                                    De même, l'utilisation de systèmes de <b>synchronisation Cloud</b> est nécéssaire pour travailler en coopération sur le même projet. Dans un premier temps, l'équipe s'est tournée vers le <b>système intégré à Unity de "Collaboration"</b>. Cela permet de simplement partager l'avancée de chacun au cours d'une séance de travail.
                                    <br><br> Depuis peu, l'équipe utilise <b>SourceTree avec BitBucket</b>, permettant de travailler à une plus grande échelle tout en <b>protégeant la production</b> au cours du développement. Tous ces outils sont ensuite utilisés dans <b>mes projets personnels ou dans mes GameJams</b> avec d'autres créatifs.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="details_single_img">
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <div class="thumb">
                                    <img src="/img/details/23.PNG" alt="">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="thumb">
                                    <img src="/img/details/24.PNG" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- discuss_projects_start  -->
    <div class="discuss_projects">
        <div class="outline_text white project d-none d-lg-block">
            <img src="/img/bg.svg" alt="">
        </div>
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="project_text text-center">
                        <h3>Contactez-moi pour commencer à travailler ensemble.</h3>
                        <a class="boxed-btn3" href="<?= $language . "#contact" ?>">Me contacter</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- discuss_projects_end  -->
    <!-- footer start -->
    <footer class="footer">
        <div class="copy-right_text">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <p class="copy_right text-center">
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--/ footer end  -->
    <!-- JS here -->
    <script src="js/vendor/modernizr-3.5.0.min.js"></script>
    <script src="js/vendor/jquery-1.12.4.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/isotope.pkgd.min.js"></script>
    <script src="js/ajax-form.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <script src="js/scrollIt.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/nice-select.min.js"></script>
    <script src="js/jquery.slicknav.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/gijgo.min.js"></script>
    <!--contact js-->
    <script src="js/contact.js"></script>
    <script src="js/jquery.ajaxchimp.min.js"></script>
    <script src="js/jquery.form.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    <script src="js/mail-script.js"></script>
    <script src="js/main.js"></script>
</body>
</html>