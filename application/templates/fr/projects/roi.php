<!doctype html>
<html class="no-js" lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Vive le Roi Détails</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta property="og:image" content="img/thumb.png">
        <meta property="og:image:type" content="image/png">
        <meta property="og:image:width" content="1920">
        <meta property="og:image:height" content="1080">
        <!-- <link rel="manifest" href="site.webmanifest"> -->
        <!-- Place favicon.ico in the root directory -->
        <link rel="stylesheet" href="/css/style.css">
    </head>
    <body>
        <!-- CSS here -->
        <link rel="stylesheet" href="/css/slicknav.css">
        <link rel="stylesheet" href="/css/bootstrap.min.css">
        <link rel="stylesheet" href="/css/font-awesome.min.css">
        <!-- <link rel="stylesheet" href="/css/responsive.css"> -->
        <!--[if lte IE 9]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->
        <!-- header-start -->
        <header>
            <div class="header-area ">
                <div id="sticky-header" class="main-header-area">
                    <div class="container-fluid">
                        <div class="row align-items-center">
                            <div class="col-xl-3 col-lg-2">
                                <div class="logo">
                                    <a href="<?= $language ?>">
                                        <img src="/img/logo.svg" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="col-xl-9 col-lg-10">
                                <div class="main-menu  d-none d-lg-block text-right">
                                    <nav>
                                        <ul id="navigation">
                                            <li><a class="active" href="<?= $language ?>">Portofio</a></li>
                                            <li><a href="<?= $language . "#contact" ?>">Contact</a></li>
                                            <li><a href="<?= $language . "/about" ?>">A propos</a></li>
                                            <li class="font-awesome">
												<a href="<?= "/" . $otherLangugage . "/projects/vive-le-roi" ?>"><i class="fas fa-flag"></i> <?= strtoupper($otherLangugage) ?></a>
											</li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="mobile_menu d-block d-lg-none"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- header-end -->
        <!-- bradcam_area  -->
        <div class="bradcam_area">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="bradcam_text text-center">
                            <h3>Vive le Roi</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /bradcam_area  -->
        <div class="work_details_area">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-10">
                        <div class="details_catagory_wrap">
                            <div class="single_catagory">
                                <span>Client</span>
                                <h4>UPEM / Gustave Eiffel</h4>
                            </div>
                            <div class="single_catagory">
                                <span>Categorie</span>
                                <h4>Jeu de société - Psychologique</h4>
                            </div>
                            <div class="single_catagory">
                                <span>Date du projet</span>
                                <h4> 2017 - 2018</h4>
                            </div>
                            <div class="single_catagory">
                                <span>Stade de création</span>
                                <div class="social_links">
                                    <h4>Beta</h4>
                                </div>
                            </div>
                        </div>
                        <div class="banner">
                            <img src="/img/details/Roi_details.png" alt="">
                        </div>
                        <div class="details_info">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="info">
                                        <p>Vous êtes un citoyen de la capitale de Hammerfall, ville <b>en plein renversement politique</b>. Le Roi ayant vu la révolution de son peuple éclater dans les rues, decide de <b>s’enfuir et de se dissimuler parmi ses concitoyens</b>. La guerre civile faisant alors rage, la seule peur des <b>revolutionnaires est de voir le Roi retourner sur le trône</b>, ils feront tout ce qui est en leurs pouvoirs pour <b>le retrouver et disposer de lui</b>. Les Royalistes n’attendent que le retour de leurs privilèges et tenteront de mater la révolution pour <b>faire revenir leur souverain dans son château</b>.
                                        <br><br>Seulement, qui sait si son voisin n’est pas <b>une raclure de Revolutionnaire ou un pourri gaté de Royaliste</b>. À vous de le decouvrir et n’oubliez pas, gardez les apparences et <br><b>VIVE LE ROI!</b></p>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="info">
                                        <p>Vive le Roi est un jeu qui construit <b>un environnement de conflit et de discorde</b> où les joueurs développent leur <b>confiance ou méfiance</b> envers leurs camarades de jeu. L'objectif du gameplay est de créer <b>des conditions propices à la discussion</b> pour inviter les joueurs à intéragir les uns avec les autres.
                                            <br><br> Le jeu a un gameplay discret pour permettre de faciliter la construction de relation entre les joueurs. L'objectif de Vive le Roi est de maintenir l'intérêt de la partie <b>même pour les joueurs éliminés, avec un système de rôle</b>. De l'autre, <b>le jeu a un système d'armes avec différentes distances</b> permettant d'attaquer certains joueurs à porter de soi.
                                        <br><br> Enfin, la révolution faisant rage, <b>un "Jeton de suspicion"</b> circule parmi les joueurs. <b>N'attirez pas trop l'attention sur vous</b>, vous risquez d'être découvert par les plus agressifs des Révolutionnaires.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="details_single_img">
                            <div class="row">
                                <div class="col-lg-4 col-md-4">
                                    <div class="thumb">
                                        <img src="/img/details/3.png" alt="">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="thumb">
                                        <img src="/img/details/4.png" alt="">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="thumb">
                                        <img src="/img/details/5.png" alt="">
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12">
                                    <div class="thumb">
                                        <img src="/img/details/6.png" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- discuss_projects_start  -->
        <div class="discuss_projects">
            <div class="outline_text white project d-none d-lg-block">
                <img src="/img/bg.svg" alt="">
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="project_text text-center">
                            <h3>Contactez-moi pour commencer à travailler ensemble.</h3>
                            <a class="boxed-btn3" href="<?= $language . "#contact" ?>">Me contacter</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- discuss_projects_end  -->
        <!-- footer start -->
        <footer class="footer">
            <div class="copy-right_text">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <p class="copy_right text-center">
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!--/ footer end  -->
        <!-- JS here -->
        <script src="js/vendor/modernizr-3.5.0.min.js"></script>
        <script src="js/vendor/jquery-1.12.4.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/isotope.pkgd.min.js"></script>
        <script src="js/ajax-form.js"></script>
        <script src="js/waypoints.min.js"></script>
        <script src="js/jquery.counterup.min.js"></script>
        <script src="js/imagesloaded.pkgd.min.js"></script>
        <script src="js/scrollIt.js"></script>
        <script src="js/jquery.scrollUp.min.js"></script>
        <script src="js/wow.min.js"></script>
        <script src="js/nice-select.min.js"></script>
        <script src="js/jquery.slicknav.min.js"></script>
        <script src="js/jquery.magnific-popup.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/gijgo.min.js"></script>
        <!--contact js-->
        <script src="js/contact.js"></script>
        <script src="js/jquery.ajaxchimp.min.js"></script>
        <script src="js/jquery.form.js"></script>
        <script src="js/jquery.validate.min.js"></script>
        <script src="js/mail-script.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>