<!doctype html>
<html class="no-js" lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>You-Manity Détails</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta property="og:image" content="img/thumb.png">
        <meta property="og:image:type" content="image/png">
        <meta property="og:image:width" content="1920">
        <meta property="og:image:height" content="1080">
        <!-- <link rel="manifest" href="site.webmanifest"> -->
        <!-- Place favicon.ico in the root directory -->
        <link rel="stylesheet" href="/css/style.css">
    </head>
    <body>
        <!-- CSS here -->
        <link rel="stylesheet" href="/css/slicknav.css">
        <link rel="stylesheet" href="/css/bootstrap.min.css">
        <link rel="stylesheet" href="/css/font-awesome.min.css">
        <!-- <link rel="stylesheet" href="/css/responsive.css"> -->
        <!--[if lte IE 9]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->
        <!-- header-start -->
        <header>
            <div class="header-area ">
                <div id="sticky-header" class="main-header-area">
                    <div class="container-fluid">
                        <div class="row align-items-center">
                            <div class="col-xl-3 col-lg-2">
                                <div class="logo">
                                    <a href="<?= $language ?>">
                                        <img src="/img/logo.svg" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="col-xl-9 col-lg-10">
                                <div class="main-menu  d-none d-lg-block text-right">
                                    <nav>
                                        <ul id="navigation">
                                            <li><a class="active" href="<?= $language ?>">Portofio</a></li>
                                            <li><a href="<?= $language . "#contact" ?>">Contact</a></li>
                                            <li><a href="<?= $language . "/about" ?>">A propos</a></li>
                                            <li class="font-awesome">
												<a href="<?= "/" . $otherLangugage . "/projects/you-manity" ?>"><i class="fas fa-flag"></i> <?= strtoupper($otherLangugage) ?></a>
											</li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="mobile_menu d-block d-lg-none"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- header-end -->
        <!-- bradcam_area  -->
        <div class="bradcam_area">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="bradcam_text text-center">
                            <h3>You-Manity</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /bradcam_area  -->
        <div class="work_details_area">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-10">
                        <div class="details_catagory_wrap">
                            <div class="single_catagory">
                                <span>Client</span>
                                <h4>UPEM / Gustave Eiffel</h4>
                            </div>
                            <div class="single_catagory">
                                <span>Categorie</span>
                                <h4>Jeu narratif - Anthroposcène</h4>
                            </div>
                            <div class="single_catagory">
                                <span>Date du projet</span>
                                <h4>Septembre 2019 - Janvier 2020</h4>
                            </div>
                            <div class="single_catagory">
                                <span>Testez le ici !</span>
                                <div class="social_links">
                                    <a href="games/You-Manity.html"> <h4>You-Manity</h4> </a>
                                </div>
                            </div>
                        </div>
                        <div class="banner">
                            <img src="/img/details/You-Manity_details.png" alt="">
                        </div>
                        <div class="details_info">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="info">
                                        <p>Le personnage incarné par le joueur est <b>un enfant né “in vitro”</b> qui va grandir au cours d’une gestation accélérée. Les laboratoires You-Manity ont fermé depuis longtemps. Cependant, il reste un enfant dans <b>les chambres cryogéniques</b> qui se fait réanimer alors que tout semblait être éteint. Le joueur comme l’enfant ouvrent donc les yeux dans ce <b>centre de formation</b> et vont découvrir tout ce que l’humanité a réussi à accomplir.
                                        <br><br>L’enfant se trouve dans une <b>“cuve de maturation”</b> dans laquelle il va pouvoir interagir avec son compagnon éducatif <b>“U-MAIN”</b>. U-MAIN est une intelligence artificielle programmée pour former les enfants “in vitro” du centre You-Manity afin d’en faire des <b>citoyens modèles et autonomes</b>. L'intelligence artificielle va faire traverser le “cocon” de joueur, de salle en salle, pour <b>lui inculquer les notions jugées primordiales pour devenir un humain exemplaire</b>. Il sera amené à découvrir de quoi se compose les repas des humains et ce qu’il faut ou ne faut pas faire.</p>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="info">
                                        <p>You-Manity est un jeu qui se joue <b>à la souris</b>, créé en <b>solitaire</b>. Incarnez un jeune enfant dans sa découverte du monde et informez vous sur ce que <b>les humains du passé on fait</b>. U-MAIN va continuer de vous accompagner jusqu'à la surface où vous sera présenté <b>l’environnement dans lequel vous allez passer le reste de votre vie</b>. La question est de savoir quel monde les humains du passé ont laissé pour vous.
                                            <br><br> Le projet <b>n'est pas terminé</b>, il fut arrêté au premier semestre 2019. Il a néanmoins fait place à un autre projet en coopération avec <a target="_blank" href="https://www.instagram.com/saltea.png/">Popielarz Manon</a> : <a target="_blank" href="">Spirited Debate.</a> </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="details_single_img">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6">
                                        <div class="thumb">
                                            <img src="/img/details/2.png" alt="">
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                    	<div class="thumb">
                                        <img src="/img/details/robot.png" alt="">
                                    	</div>
                                    </div>
                                </div>
                                <div class="row">
                                	<div class="col-lg-12 col-md-12">
                                        <div class="thumb d-flex justify-content-center">
                                            <iframe width="1280" height="720" src="https://www.youtube-nocookie.com/embed/NtKEGZM3Udc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- discuss_projects_start  -->
            <div class="discuss_projects">
                <div class="outline_text white project d-none d-lg-block">
                    <img src="/img/bg.svg" alt="">
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="project_text text-center">
                                <h3>Contactez-moi pour commencer à travailler ensemble.</h3>
                                <a class="boxed-btn3" href="<?= $language . "#contact" ?>">Me contacter</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- discuss_projects_end  -->
            <!-- footer start -->
            <footer class="footer">
                <div class="copy-right_text">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-12">
                                <p class="copy_right text-center">
                                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <!--/ footer end  -->
            <!-- JS here -->
            <script src="js/vendor/modernizr-3.5.0.min.js"></script>
            <script src="js/vendor/jquery-1.12.4.min.js"></script>
            <script src="js/popper.min.js"></script>
            <script src="js/bootstrap.min.js"></script>
            <script src="js/owl.carousel.min.js"></script>
            <script src="js/isotope.pkgd.min.js"></script>
            <script src="js/ajax-form.js"></script>
            <script src="js/waypoints.min.js"></script>
            <script src="js/jquery.counterup.min.js"></script>
            <script src="js/imagesloaded.pkgd.min.js"></script>
            <script src="js/scrollIt.js"></script>
            <script src="js/jquery.scrollUp.min.js"></script>
            <script src="js/wow.min.js"></script>
            <script src="js/nice-select.min.js"></script>
            <script src="js/jquery.slicknav.min.js"></script>
            <script src="js/jquery.magnific-popup.min.js"></script>
            <script src="js/plugins.js"></script>
            <script src="js/gijgo.min.js"></script>
            <!--contact js-->
            <script src="js/contact.js"></script>
            <script src="js/jquery.ajaxchimp.min.js"></script>
            <script src="js/jquery.form.js"></script>
            <script src="js/jquery.validate.min.js"></script>
            <script src="js/mail-script.js"></script>
            <script src="js/main.js"></script>
        </body>
    </html>