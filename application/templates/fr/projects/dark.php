<!doctype html>
<html class="no-js" lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Dark School Détails</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta property="og:image" content="img/thumb.png">
        <meta property="og:image:type" content="image/png">
        <meta property="og:image:width" content="1920">
        <meta property="og:image:height" content="1080">
        <!-- <link rel="manifest" href="site.webmanifest"> -->
        <!-- Place favicon.ico in the root directory -->
        <link rel="stylesheet" href="/css/style.css">
    </head>
    <body>
        <!-- CSS here -->
        <link rel="stylesheet" href="/css/slicknav.css">
        <link rel="stylesheet" href="/css/bootstrap.min.css">
        <link rel="stylesheet" href="/css/font-awesome.min.css">
        
        <!-- <link rel="stylesheet" href="/css/responsive.css"> -->
        <!--[if lte IE 9]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->
        <!-- header-start -->
        <header>
            <div class="header-area ">
                <div id="sticky-header" class="main-header-area">
                    <div class="container-fluid">
                        <div class="row align-items-center">
                            <div class="col-xl-3 col-lg-2">
                                <div class="logo">
                                    <a href="<?= $language ?>">
                                        <img src="/img/logo.svg" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="col-xl-9 col-lg-10">
                                <div class="main-menu  d-none d-lg-block text-right">
                                    <nav>
                                        <ul id="navigation">
                                            <li><a class="active" href="<?= $language ?>">Portofio</a></li>
                                            <li><a href="<?= $language . "#contact" ?>">Contact</a></li>
                                            <li><a href="<?= $language . "/about" ?>">A propos</a></li>
                                            <li class="font-awesome">
                                            <a href="<?= "/" . $otherLangugage . "/projects/dark-school"?>"><i class="fas fa-flag"></i> <?= strtoupper($otherLangugage) ?></a>
											</li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="mobile_menu d-block d-lg-none"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- header-end -->
        <!-- bradcam_area  -->
        <div class="bradcam_area">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="bradcam_text text-center">
                            <h3>Dark School</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /bradcam_area  -->
        <div class="work_details_area">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-10">
                        <div class="details_catagory_wrap">
                            <div class="single_catagory">
                                <span>Client</span>
                                <h4>UPEM / Gustave Eiffel</h4>
                            </div>
                            <div class="single_catagory">
                                <span>Categorie</span>
                                <h4>Jeu à choix - horreur</h4>
                            </div>
                            <div class="single_catagory">
                                <span>Date du projet</span>
                                <h4>Janvier 2019 - Mars 2019</h4>
                            </div>
                            <div class="single_catagory">
                                <a class="boxed-btn3 porject_btn" target="_blank" href="http://projets-arts-numeriques.eklablog.com/dark-schools-a161981846">Lien vers le Projet</a>
                            </div>
                        </div>
                        <div class="banner">
                            <img src="/img/details/Dark_details.PNG" alt="">
                        </div>
                        <div class="details_info">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="info">
                                        <p>Pour Dante, l'Enfer est composé de <bv>neuf formes circulaires</b> qui se superposent les unes sur les autres, rivalisant en matière de châtiments jusqu'à atteindre <b>le neuvième et dernier cercle où siège Lucifer</b>, le prince des Ténèbres. Pour un élève de l'UPEM en pleine période de rendus semestriels, l'Enfer c'est cet <b>imposant bâtiment qui s'échelonne</b> sur trois étages constitués de salles de classe. Un enfer où il faut lutter pour garder les yeux ouverts, préserver sa motivation et ne <b>jamais se laisser décourager</b> pour enfin arriver aux vacances d'été tant désirées. Cet enfer c'est neuf mois de dur labeur où les <b>journées s'enchaînent et se ressemblent</b> et où la moindre échappatoire est bonne à prendre.
                                            <br><br>
                                            <b>Dark Schools</b>, c'est un aperçu quelque peu surnaturel de notre vision personnelle de l'Enfer. C'est le <b>quotidien d'un étudiant</b> luttant pour sa survie physique mais surtout <b>psychologique</b> et, où chaque choix s'avère décisif. Inspiré du <b>RPG</b> (role playing game) qui lui est presque éponyme, <b>Dark Souls</b>, ce FMV (full motion video) composé de <b>séquences filmées</b> et de séquences "d'actions" dites <b>QTE</b> (quick time event) précipite le spectateur à la place d'acteur et le pousse à agir et interagir pour sauver sa peau et parfois même celle de ses amis…
                                        </p>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="info">
                                        <p>Dans une ambiance <b>sombre et angoissante</b>, Dark Schools propose de transposer le spectateur <b>dans les bottes de ce fameux étudiant</b> et de lui faire passer une journée à l'université dans ce qui s'avère être une véritable descente aux Enfers. Par le biais de <b>capteurs</b>, il lui faudra avancer et surmonter les nombreuses difficultés, le tout dans <b>un laps de temps qui lui est compté</b>. L'utilisation de ces capteurs est indiquée par de <b>petites icônes</b> qui apparaissent sur l'écran au moment opportun.
                                            <br><br>
                                            L'utilisateur n'a plus qu'à faire preuve de <b>réflexes et de rapidité</b> pour se dépêtrer des nombreuses situations dans lesquelles il se retrouve plongé.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="details_single_img">
                            <div class="row">
                                <div class="col-lg-6 col-md-6">
                                    <div class="thumb">
                                        <img src="/img/details/8.jpg" alt="">
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="thumb">
                                        <img src="/img/details/7.jpg" alt="">
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12">
                                    <div class="thumb d-flex justify-content-center">
                                        <iframe width="1280" height="720" src="https://www.youtube-nocookie.com/embed/zxGmTB0TR1c" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!--/ service_area  -->
        <div class="creative_people">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section_title text-center">
                            <h4>Les professionnels sur Dark School</h4>
                        </div>
                    </div>
                </div>
                <div class="border_bottom">
                    <div class="row">
                        <div class="col-lg-3 col-md-6">
                            <div class="single_people">
                                <div class="thumb">
                                    <a target="_blank" href="https://www.instagram.com/saltea.png/"><img src="/img/creative_blog/author2.png" alt="">
                                    </div>
                                    <div class="people_info">
                                        <h4>Manon POPIELARZ</h4>
                                        <p>Sound Designer, Photographe, Monteuse</p></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6">
                                <div class="single_people">
                                    <div class="thumb">
                                        <a target="_blank" href="https://www.artstation.com/steelar_bubbles">
                                            <img src="/img/creative_blog/author3.png" alt="">
                                        </div>
                                        <div class="people_info">
                                            <h4>Simon SIGAL</h4>
                                            <p>FX Artist, Environnement artist</p></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6">
                                    <div class="single_people">
                                        <div class="thumb">
                                            <a target="_blank" href="https://www.linkedin.com/in/agathe-bosch?fbclid=IwAR0koP27g0Ek_NiPLN5pWQk4T0oRzM30PPhNP688KVLQbdNa39N6vkTN4S8">
                                                <img src="/img/creative_blog/agathe.jpg" alt="">
                                            </div>
                                            <div class="people_info">
                                                <h4>Agathe BOSCH COZZOLINO</h4>
                                                <p>Infographist, Digital Artist and Web Designer</p></a>
                                                <a target="_blank" href="https://www.artstation.com/caligula_art?fbclid=IwAR14zst_OFtZnFQmDGHMbux_8-LkK9QwTvOZBq70H7JlfpDEv2sdbZwPRUo"><p><b>Artstation</b></p></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-6">
                                        <div class="single_people">
                                            <div class="thumb">
                                                <a target="_blank" href="https://fr.linkedin.com/in/thomas-escoffier-a4aa2917a">
                                                    <img src="/img/creative_blog/author5.png" alt="">
                                                </div>
                                                <div class="people_info">
                                                    <h4>Thomas Escoffier</h4>
                                                    <p>Game Designer, Level Designer et Programmeur</p></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- discuss_projects_start  -->
                        <div class="discuss_projects">
                            <div class="outline_text white project d-none d-lg-block">
                                <img src="/img/bg.svg" alt="">
                            </div>
                            <div class="container">
                                <div class="row">
                                    <div class="col-xl-12">
                                        <div class="project_text text-center">
                                            <h3>Contactez-moi pour commencer à travailler ensemble.</h3>
                                            <a class="boxed-btn3" href="<?= $language . "#contact" ?>">Me contacter</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- discuss_projects_end  -->
                        <!-- footer start -->
                        <footer class="footer">
                            <div class="copy-right_text">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-xl-12">
                                            <p class="copy_right text-center">
                                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                                Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </footer>
                        <!--/ footer end  -->
                        <!-- JS here -->
                        <script src="js/vendor/modernizr-3.5.0.min.js"></script>
                        <script src="js/vendor/jquery-1.12.4.min.js"></script>
                        <script src="js/popper.min.js"></script>
                        <script src="js/bootstrap.min.js"></script>
                        <script src="js/owl.carousel.min.js"></script>
                        <script src="js/isotope.pkgd.min.js"></script>
                        <script src="js/ajax-form.js"></script>
                        <script src="js/waypoints.min.js"></script>
                        <script src="js/jquery.counterup.min.js"></script>
                        <script src="js/imagesloaded.pkgd.min.js"></script>
                        <script src="js/scrollIt.js"></script>
                        <script src="js/jquery.scrollUp.min.js"></script>
                        <script src="js/wow.min.js"></script>
                        <script src="js/nice-select.min.js"></script>
                        <script src="js/jquery.slicknav.min.js"></script>
                        <script src="js/jquery.magnific-popup.min.js"></script>
                        <script src="js/plugins.js"></script>
                        <script src="js/gijgo.min.js"></script>
                        <!--contact js-->
                        <script src="js/contact.js"></script>
                        <script src="js/jquery.ajaxchimp.min.js"></script>
                        <script src="js/jquery.form.js"></script>
                        <script src="js/jquery.validate.min.js"></script>
                        <script src="js/mail-script.js"></script>
                        <script src="js/main.js"></script>
                    </body>
                </html>