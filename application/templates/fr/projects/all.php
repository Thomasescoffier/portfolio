<!doctype html>
<html class="no-js" lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Tous mes travaux</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta property="og:image" content="img/thumb.png">
        <meta property="og:image:type" content="image/png">
        <meta property="og:image:width" content="1920">
        <meta property="og:image:height" content="1080">
        <!-- <link rel="manifest" href="site.webmanifest"> -->
        <!-- Place favicon.ico in the root directory -->
        <link rel="stylesheet" href="/css/style.css">
    </head>
    <body>
        <!-- CSS here -->
        <link rel="stylesheet" href="/css/slicknav.css">
        <link rel="stylesheet" href="/css/bootstrap.min.css">
        <link rel="stylesheet" href="/css/font-awesome.min.css">
        <!-- <link rel="stylesheet" href="/css/responsive.css"> -->
        <!--[if lte IE 9]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->
        <!-- header-start -->
        <header>
            <div class="header-area ">
                <div id="sticky-header" class="main-header-area">
                    <div class="container-fluid">
                        <div class="row align-items-center">
                            <div class="col-xl-3 col-lg-2">
                                <div class="logo">
                                    <a href="<?= $language ?>">
                                        <img src="/img/logo.svg" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="col-xl-9 col-lg-10">
                                <div class="main-menu  d-none d-lg-block text-right">
                                    <nav>
                                        <ul id="navigation">
                                            <li><a class="active" href="<?= $language ?>">Portofio</a></li>
                                            <li><a href="<?= $language . "#contact" ?>">Contact</a></li>
                                            <li><a href="<?= $language . "/about" ?>">A propos</a></li>
                                            <li class="font-awesome">
                                            <a href="<?= "/" . $otherLangugage . "/projects" ?>"><i class="fas fa-flag"></i> <?= strtoupper($otherLangugage) ?></a>
											</li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="mobile_menu d-block d-lg-none"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- header-end -->
        <!-- bradcam_area  -->
        <div class="bradcam_area">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="bradcam_text text-center">
                            <h3>Tous mes travaux</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /bradcam_area  -->
   <!-- slider_area_end -->
        <div class="portfolio_area blue" id="projects">
            <!-- <div class="container-fluid p-0"> -->
            <div class="portfolio_wrap">
                <a href="<?= $language . "/projects/emily" ?>">
                    <div class=" single_gallery">
                        
                        <div class="thumb">
                            <img src="/img/portfolio/1.svg" alt="">
                        </div>
                        <div class="gallery_hover">
                            <div class="hover_inner">
                                <span>Projet Universitaire sur Unity 3D</span>
                                <h3>Emily's Friend</h3>
                                
                            </div>
                        </div>
                    </div>
                </a>
                
                <a href="<?= $language . "/projects/you-manity" ?>">
                    <div class="single_gallery">
                        
                        <div class="thumb">
                            <img src="/img/portfolio/2.svg" alt="">
                        </div>
                        <div class="gallery_hover">
                            <div class="hover_inner">
                                <span>Projet Universitaire sur Unity 3D</span>
                                <h3>You-Manity</h3>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="<?= $language . "/projects/vive-le-roi" ?>">
                    <div class="single_gallery">
                        <div class="thumb">
                            <img src="/img/portfolio/3.svg" alt="">
                        </div>
                        <div class="gallery_hover">
                            <div class="hover_inner">
                                <span>Projet Universitaire de jeu de société</span>
                                <h3>Vive le Roi !</h3>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="portfolio_wrap orange">
                <a href="<?= $language . "/projects/dark-school" ?>">
                    <div class=" single_gallery">
                        <div class="thumb">
                            <img src="/img/portfolio/4.svg" alt="">
                        </div>
                        <div class="gallery_hover">
                            <div class="hover_inner">
                                <span>Projet Universitaire sur Max 7 Cycling </span>
                                <h3>Dark School <br> (Installation)</h3>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="<?= $language . "/projects/organisation" ?>">
                    <div class=" single_gallery">
                        <div class="thumb">
                            <img src="/img/portfolio/11.svg" alt="">
                        </div>
                        <div class="gallery_hover">
                            <div class="hover_inner">
                                <span>Gestion d'équipe dans les Projet Universitaire </span>
                                <h3>Organisation de projet <br> (Excel/GitHub)</h3>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="<?= $language . "/projects/internship-hexaverse" ?>">
                    <div class="single_gallery">
                        
                        <div class="thumb">
                            <img src="/img/portfolio/5.svg" alt="">
                        </div>
                        <div class="gallery_hover">
                            <div class="hover_inner">
                                <span>Expérience de stage : Game Design</span>
                                <h3>Hexaverse</h3>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="<?= $language . "/projects/zappening" ?>">
                    <div class=" single_gallery">
                        <div class="thumb">
                            <img src="/img/portfolio/6.svg" alt="">
                        </div>
                        <div class="gallery_hover">
                            <div class="hover_inner">
                                <span>Projets Universitaires sur After Effects</span>
                                <h3>Zappening</h3>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="portfolio_wrap blue">
                <a href="<?= $language . "/projects/graphic-design" ?>">
                    <div class="single_gallery">
                        <div class="thumb">
                            <img src="/img/portfolio/7.svg" alt="">
                        </div>
                        <div class="gallery_hover">
                            <div class="hover_inner">
                                <span>Projets Personnels sur Illustrator</span>
                                <h3>Infographie</h3>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="<?= $language . "/projects/dungeon-and-dragon" ?>">
                    <div class=" single_gallery">
                        <div class="thumb">
                            <img src="/img/portfolio/8.svg" alt="">
                        </div>
                        <div class="gallery_hover">
                            <div class="hover_inner">
                                <span>Projets Personnels : Level Design </span>
                                <h3>Donjon et Dragon</h3>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="<?= $language . "/projects/gamejams" ?>">
                    <div class=" single_gallery">
                        <div class="thumb">
                            <img src="/img/portfolio/9.svg" alt="">
                        </div>
                        <div class="gallery_hover">
                            <div class="hover_inner">
                                <span>Projets Personnels : Game Design</span>
                                <h3>Game Jam</h3>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <!-- </div> -->
        </div>
            <!-- discuss_projects_start  -->
            <div class="discuss_projects">
                <div class="outline_text white project d-none d-lg-block">
                    <img src="/img/bg.svg" alt="">
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="project_text text-center">
                                <h3>Contactez-moi pour commencer à travailler ensemble.</h3>
                                <a class="boxed-btn3" href="<?= $language . "#contact" ?>">Me contacter</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- discuss_projects_end  -->
            <!-- footer start -->
            <footer class="footer">
                <div class="copy-right_text">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-12">
                                <p class="copy_right text-center">
                                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <!--/ footer end  -->
            <!-- JS here -->
            <script src="js/vendor/modernizr-3.5.0.min.js"></script>
            <script src="js/vendor/jquery-1.12.4.min.js"></script>
            <script src="js/popper.min.js"></script>
            <script src="js/bootstrap.min.js"></script>
            <script src="js/owl.carousel.min.js"></script>
            <script src="js/isotope.pkgd.min.js"></script>
            <script src="js/ajax-form.js"></script>
            <script src="js/waypoints.min.js"></script>
            <script src="js/jquery.counterup.min.js"></script>
            <script src="js/imagesloaded.pkgd.min.js"></script>
            <script src="js/scrollIt.js"></script>
            <script src="js/jquery.scrollUp.min.js"></script>
            <script src="js/wow.min.js"></script>
            <script src="js/nice-select.min.js"></script>
            <script src="js/jquery.slicknav.min.js"></script>
            <script src="js/jquery.magnific-popup.min.js"></script>
            <script src="js/plugins.js"></script>
            <script src="js/gijgo.min.js"></script>
            <!--contact js-->
            <script src="js/contact.js"></script>
            <script src="js/jquery.ajaxchimp.min.js"></script>
            <script src="js/jquery.form.js"></script>
            <script src="js/jquery.validate.min.js"></script>
            <script src="js/mail-script.js"></script>
            <script src="js/main.js"></script>
        </body>
    </html>