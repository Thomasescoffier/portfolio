<!doctype html>
<html class="no-js" lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Emily's Friend Détails</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta property="og:image" content="img/thumb.png">
        <meta property="og:image:type" content="image/png">
        <meta property="og:image:width" content="1920">
        <meta property="og:image:height" content="1080">
        <!-- <link rel="manifest" href="site.webmanifest"> -->
        <!-- Place favicon.ico in the root directory -->
        <link rel="stylesheet" href="/css/style.css">
    </head>
    <body>
        <!-- CSS here -->
        <link rel="stylesheet" href="/css/slicknav.css">
        <link rel="stylesheet" href="/css/bootstrap.min.css">
        <link rel="stylesheet" href="/css/font-awesome.min.css">
        <!-- <link rel="stylesheet" href="/css/responsive.css"> -->
        <!--[if lte IE 9]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->
        <!-- header-start -->
        <header>
            <div class="header-area ">
                <div id="sticky-header" class="main-header-area">
                    <div class="container-fluid">
                        <div class="row align-items-center">
                            <div class="col-xl-3 col-lg-2">
                                <div class="logo">
                                    <a href="<?= $language ?>">
                                        <img src="/img/logo.svg" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="col-xl-9 col-lg-10">
                                <div class="main-menu  d-none d-lg-block text-right">
                                    <nav>
                                        <ul id="navigation">
                                            <li><a class="active" href="<?= $language ?>">Portofio</a></li>
                                            <li><a href="<?= $language . "#contact" ?>">Contact</a></li>
                                            <li><a href="about.html">A propos</a></li>
                                            <li class="font-awesome">
												<a href="<?= "/" . $otherLangugage . "/projects/emily" ?>"><i class="fas fa-flag"></i> <?= strtoupper($otherLangugage) ?></a>
											</li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="mobile_menu d-block d-lg-none"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- header-end -->
        <!-- bradcam_area  -->
        <div class="bradcam_area">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="bradcam_text text-center">
                            <h3>Emily's Friend</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /bradcam_area  -->
        <div class="work_details_area">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-10">
                        <div class="details_catagory_wrap">
                            <div class="single_catagory">
                                <span>Client</span>
                                <h4>UPEM / Gustave Eiffel</h4>
                            </div>
                            <div class="single_catagory">
                                <span>Categorie</span>
                                <h4>Jeu PC - Aventure</h4>
                            </div>
                            <div class="single_catagory">
                                <span>Date du projet</span>
                                <h4>Septembre 2019 - Avril 2020</h4>
                            </div>
                            <div class="single_catagory">
                                <span>Téléchargement</span>
                                <div class="social_links">
                                    <a href="#"> <h4>Itch.io</h4> </a>
                                </div>
                            </div>
                            <div class="single_catagory">
                                <a class="boxed-btn3 porject_btn" target="_blank" href="https://greenpeas.trashmates.fr/">Lien vers le Projet</a>
                            </div>
                        </div>
                        <div class="banner">
                            <img src="/img/details/Emily_details.png" alt="">
                        </div>
                        <div class="details_info">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="info">
                                        <p>Emily est une <b>jeune fille de 9 ans et demi</b>, vivant avec son père John. Un jour, alors qu’Emily joue dans son jardin, elle entend un petit bruit dans les poubelles à côté de la maison. Elle découvre alors <b>un petit écureuil</b>, fouillant les déchets en quête de nourriture. La jeune fille s'empresse de lui donner des glands jonchant le sol, que le petit animal accepta.
                                        <br><br>Alors qu’Emily se fait un nouvel ami, <b>John sort dans le jardin</b> pour surprendre Emily avec <b>un animal sauvage</b> sur l’épaule. L'écureuil voyant le père arriver, s'enfuit dans l’arbre planté dans le jardin et se cacha de la vue de tous. Lui donnant affectueusement <b>le nom de Tofu</b>, Emily va devoir aider son nouvel ami à trouver de quoi se nourrir car <b>l’hiver vient</b>. Néanmoins, la présence de Tofu doit rester <b>secrete aux yeux de son père.</b></p>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="info">
                                        <p>Emily's Friend est un projet de jeu PC étudiant qui se joue <b>au clavier ou à la manette</b>, créé par une équipe de <b>cinq personnes</b>. Accompagnez Emily dans un périple dangereux <b>en quête de nourriture pour Tofu</b>. La jeune fille devra rester brave face <b>aux pollutions visuelles, sonores et olfactives</b> qui jonchent sa route.<br><br> Vous devez protéger Emily au cours de son aventure en évitant qu'elle soit <b>affectée par son environnement et tombe en larme au sol...</b> Heureusement que la <b>nature est là pour l'apaiser.</b> </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="details_single_img">
                            <div class="row">
                                <div class="col-lg-6 col-md-6">
                                    <video width="100%" height="100%" controls>
                                        <source src="/img/details/Trailer.mp4" type="video/mp4">
                                    </video>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="thumb">
                                        <img src="/img/details/1.png" alt="">
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/ service_area  -->
        <div class="creative_people">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section_title text-center">
                            <h4>Les professionnels sur Emily's Friend</h4>
                        </div>
                    </div>
                </div>
                <div class="border_bottom">
                    <div class="row">
                        <div class="col-lg-3 col-md-6">
                            <div class="single_people">
                                <div class="thumb">
                                    <a target="_blank" href="https://www.instagram.com/saltea.png/"><img src="/img/creative_blog/author2.png" alt="">
                                </div>
                                <div class="people_info">
                                    <h4>Manon Popielarz</h4>
                                    <p>Sound Designer, Photographe, Monteuse</p></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="single_people">
                                <div class="thumb">
                                    <a target="_blank" href="https://www.artstation.com/steelar_bubbles"><img src="/img/creative_blog/author3.png" alt="">
                                </div>
                                <div class="people_info">
                                    <h4>Simon Sigal</h4>
                                    <p>FX Artist, Environnement artist</p></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="single_people">
                                <div class="thumb">
                                    <a target="_blank" href="https://www.instagram.com/clementmbt_design/">
                                    <img src="/img/creative_blog/author.png" alt="">
                                </div>
                                <div class="people_info">
                                    <h4>Clément Maubert</h4>
                                    <p>Artiste Modélisateur et Animateur 3D</p></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="single_people">
                                <div class="thumb">
                                    <img src="/img/creative_blog/author4.png" alt="">
                                </div>
                                <div class="people_info">
                                    <h4>Jacklyn Pusung</h4>
                                    <p>Graphic Designer, Front-End Web Design</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="single_thomas">
                                <div class="thumbnail">
                                    <a target="_blank" href="https://fr.linkedin.com/in/thomas-escoffier-a4aa2917a">
                                    <img src="/img/creative_blog/author5.png" alt="">
                                </div>
                                <div class="people_info">
                                    <h4>Thomas Escoffier</h4>
                                    <p>Game Designer, Level Designer et Programmeur</p></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

                    <!-- discuss_projects_start  -->
                    <div class="discuss_projects">
                        <div class="outline_text white project d-none d-lg-block">
                            <img src="/img/bg.svg" alt="">
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-xl-12">
                                    <div class="project_text text-center">
                                        <h3>Contactez-moi pour commencer à travailler ensemble.</h3>
                                        <a class="boxed-btn3" href="index.html#contact">Me contacter</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- discuss_projects_end  -->
                    <!-- footer start -->
                    <footer class="footer">
                        <div class="copy-right_text">
                            <div class="container">
                                <div class="row">
                                    <div class="col-xl-12">
                                        <p class="copy_right text-center">
                                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                            Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </footer>
                    <!--/ footer end  -->
                    <!-- JS here -->
                    <script src="js/vendor/modernizr-3.5.0.min.js"></script>
                    <script src="js/vendor/jquery-1.12.4.min.js"></script>
                    <script src="js/popper.min.js"></script>
                    <script src="js/bootstrap.min.js"></script>
                    <script src="js/owl.carousel.min.js"></script>
                    <script src="js/isotope.pkgd.min.js"></script>
                    <script src="js/ajax-form.js"></script>
                    <script src="js/waypoints.min.js"></script>
                    <script src="js/jquery.counterup.min.js"></script>
                    <script src="js/imagesloaded.pkgd.min.js"></script>
                    <script src="js/scrollIt.js"></script>
                    <script src="js/jquery.scrollUp.min.js"></script>
                    <script src="js/wow.min.js"></script>
                    <script src="js/nice-select.min.js"></script>
                    <script src="js/jquery.slicknav.min.js"></script>
                    <script src="js/jquery.magnific-popup.min.js"></script>
                    <script src="js/plugins.js"></script>
                    <script src="js/gijgo.min.js"></script>
                    <!--contact js-->
                    <script src="js/contact.js"></script>
                    <script src="js/jquery.ajaxchimp.min.js"></script>
                    <script src="js/jquery.form.js"></script>
                    <script src="js/jquery.validate.min.js"></script>
                    <script src="js/mail-script.js"></script>
                    <script src="js/main.js"></script>
                </body>
            </html>