<!doctype html>
<html class="no-js" lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Hexaverse Détails</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta property="og:image" content="img/thumb.png">
        <meta property="og:image:type" content="image/png">
        <meta property="og:image:width" content="1920">
        <meta property="og:image:height" content="1080">
        <!-- <link rel="manifest" href="site.webmanifest"> -->
        <!-- Place favicon.ico in the root directory -->
        <link rel="stylesheet" href="/css/style.css">
    </head>
    <body>
        <!-- CSS here -->
        <link rel="stylesheet" href="/css/slicknav.css">
        <link rel="stylesheet" href="/css/bootstrap.min.css">
        <link rel="stylesheet" href="/css/font-awesome.min.css">
        <!-- <link rel="stylesheet" href="/css/responsive.css"> -->
        <!--[if lte IE 9]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->
        <!-- header-start -->
        <header>
            <div class="header-area ">
                <div id="sticky-header" class="main-header-area">
                    <div class="container-fluid">
                        <div class="row align-items-center">
                            <div class="col-xl-3 col-lg-2">
                                <div class="logo">
                                    <a href="<?= $language ?>">
                                        <img src="/img/logo.svg" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="col-xl-9 col-lg-10">
                                <div class="main-menu  d-none d-lg-block text-right">
                                    <nav>
                                        <ul id="navigation">
                                            <li><a class="active" href="<?= $language ?>">Portofio</a></li>
                                            <li><a href="<?= $language . "#contact" ?>">Contact</a></li>
                                            <li><a href="<?= $language . "/about" ?>">A propos</a></li>
                                            <li class="font-awesome">
												<a href="<?= "/" . $otherLangugage . "/projects/internship-hexaverse" ?>"><i class="fas fa-flag"></i> <?= strtoupper($otherLangugage) ?></a>
											</li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="mobile_menu d-block d-lg-none"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- header-end -->
        <!-- bradcam_area  -->
        <div class="bradcam_area">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="bradcam_text text-center">
                            <h3>Hexaverse</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /bradcam_area  -->
        <div class="work_details_area">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-10">
                        <div class="details_catagory_wrap">
                            <div class="single_catagory">
                                <span>Client</span>
                                <h4>Sorbonne / Ikigai</h4>
                            </div>
                            <div class="single_catagory">
                                <span>Categorie</span>
                                <h4>Serious Game - Mathématique</h4>
                            </div>
                            <div class="single_catagory">
                                <span>Date du projet</span>
                                <h4>Avril 2019 - Mai 2019</h4>
                            </div>
                        </div>
                        <div class="banner">
                            <img src="/img/details/Hexa_details.png" alt="">
                        </div>
                        <div class="details_info">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="info">
                                        <p><b>HexaVerse</b> est un projet de recherche en Game Design. <b>En parallèle</b> de mon travail de </b>QA tester</b> pour <a target= "_blank" href="http://www.ikigai.games/games/gamesList">Ikigai</a>, il m'a été attribué le design d'une idée de projet : <b>Hexaverse</b>. L'objectif était de construire un jeu de <b>"Match 3" à la façon des "Candy Crush"</b> dans un but éducatif. La cible était <b>les étudiants en Master de mathématiques</b> à la <a target= "_blank" href="https://www.sorbonne-universite.fr/">Sorbonne</a>.
                                    <br><br>Le développement du <b>Game Design Document</b> fut une expérience intéressante, il s'agissait de ma première recherche de jeu dans <b>un environnement professionnel</b>. Ainsi, ne connaissant pas le monde du Math 3, une grande partie de ma recherche fut <b>d'étudier le gameplay</b> des jeux populaires de Match 3 et de comprendre les mécaniques utilisées.</p>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="info">
                                    <p>
                                        La création de HexaVerse fut une <b>révélation de ma capacité de création</b>, d'apprentissage et ma volonté de <b>formation en autodidacte</b>.
                                    <br><br> En effet, les connaissances et les notions de Game Design apprises au cours de ce stage furent <b>majoritairement maîtrisées en autodidacte</b>. cela m’a permis de sortir du cadre purement pédagogique, du rôle de stagiaire pour prendre des initiatives, donner des directives et <b>agir plus en professionnel</b>.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="details_single_img">
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <div class="thumb">
                                    <img src="/img/details/7.PNG" alt="">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="thumb">
                                    <img src="/img/details/8.PNG" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- discuss_projects_start  -->
    <div class="discuss_projects">
        <div class="outline_text white project d-none d-lg-block">
            <img src="/img/bg.svg" alt="">
        </div>
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="project_text text-center">
                        <h3>Contactez-moi pour commencer à travailler ensemble.</h3>
                        <a class="boxed-btn3" href="<?= $language . "#contact" ?>">Me contacter</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- discuss_projects_end  -->
    <!-- footer start -->
    <footer class="footer">
        <div class="copy-right_text">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <p class="copy_right text-center">
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--/ footer end  -->
    <!-- JS here -->
    <script src="js/vendor/modernizr-3.5.0.min.js"></script>
    <script src="js/vendor/jquery-1.12.4.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/isotope.pkgd.min.js"></script>
    <script src="js/ajax-form.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <script src="js/scrollIt.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/nice-select.min.js"></script>
    <script src="js/jquery.slicknav.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/gijgo.min.js"></script>
    <!--contact js-->
    <script src="js/contact.js"></script>
    <script src="js/jquery.ajaxchimp.min.js"></script>
    <script src="js/jquery.form.js"></script>
    <script src="js/jquery.validate.min.js"></script>
    <script src="js/mail-script.js"></script>
    <script src="js/main.js"></script>
</body>
</html>