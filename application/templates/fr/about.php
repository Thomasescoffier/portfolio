<!doctype html>
<html class="no-js" lang="fr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>A propos</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta property="og:image" content="img/thumb.png">
        <meta property="og:image:type" content="image/png">
        <meta property="og:image:width" content="1920">
        <meta property="og:image:height" content="1080">
        <!-- <link rel="manifest" href="site.webmanifest"> -->
        <!-- Place favicon.ico in the root directory -->
        <link rel="stylesheet" href="/css/style.css">
    </head>
    <body>
        <!-- CSS here -->
        <link rel="stylesheet" href="/css/slicknav.css">
        <link rel="stylesheet" href="/css/bootstrap.min.css">
        <link rel="stylesheet" href="/css/font-awesome.min.css">
        <!-- <link rel="stylesheet" href="/css/responsive.css"> -->
        <!--[if lte IE 9]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->
        <!-- header-start -->
        <header>
            <div class="header-area ">
                <div id="sticky-header" class="main-header-area">
                    <div class="container-fluid">
                        <div class="row align-items-center">
                            <div class="col-xl-3 col-lg-2">
                                <div class="logo">
                                    <a href="<?= $language ?>">
                                        <img src="/img/logo.svg" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="col-xl-9 col-lg-10">
                                <div class="main-menu  d-none d-lg-block text-right">
                                    <nav>
                                        <ul id="navigation">
                                            <li><a class="active" href="<?= $language ?>">Portofio</a></li>
                                            <li><a href="<?= $language . "#contact" ?>">Contact</a></li>
                                            <li><a target="_blank" href="img/about/CV 2020 Thomas ESCOFFIER.pdf">Mon CV</a></li>
                                            <li class="font-awesome">
												<a href="<?= "/" . $otherLangugage . "/about" ?>"><i class="fas fa-flag"></i> <?= strtoupper($otherLangugage) ?></a>
											</li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="mobile_menu d-block d-lg-none"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- header-end -->
        <!-- bradcam_area  -->
        <div class="bradcam_area">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="bradcam_text text-center">
                            <h3>À propos</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /bradcam_area  -->
        <div class="about_wrap_area">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-11">
                        <div class="about_heading text-center">
                            <h3>Étudiant en art, je cherche à me spécialiser en <b>Game Design et Level Design</b>. Je travaille beaucoup en autodidacte et j'aimerais trouver de nouvelles expériences pour m'améliorer </h3>
                        </div>
                    </div>
                </div>
                <div class="about_info_wrap">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="single_info">
                                <p>J'ai obtenu mon BAC S en 2015 et me suis dirigé vers des études en médecine. Après une année en PACES (première année commune des études de santé), je décide de prendre <b>une année de formation en Infographie</b> (L'ESA Games, Carpentras) qui me permet de découvrir une nouvelle voie dans l'art. Je commence alors une <b>licence en Arts Numériques</b> (EVMAN) de 2017 à 2020 </p>
                                
                                <br><br>
                                <p>Au bout de deux ans dans cette licence, je découvre le monde du Game Design à travers un projet de jeu de société : <b>Vive le Roi</b>. En trouvant ce nouveau métier, je m'y intéresse beaucoup et décide de me former <b>en autodidacte à côté de ma licence</b>. À la fin de ma 2ème année, je participe à un stage dans un studio de jeu vidéo de la Sorbonne : <a target="_blank" href="http://www.ikigai.games/games/gamesList">Ikigai</a>. J'y apprends le Game Design et le travail de <b>QA Tester</b>.
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="single_info">
                                
                                <p>Dès la 3ème année, je m'investis à temps complet dans le Game Design et dans d'autres corps de métiers comme le <b>Level Design ou le Management d'une équipe</b>. Ainsi, je travaille avec ma première équipe de développement, sur deux jeux : <b>Emily's friend ainsi que Spirited Debate</b>. En 2020, Mon objectif est de trouver de <b>nouvelles expériences dans le domaine du jeu vidéo</b> et de comprendre <b>comment s'intègre le travail de Game Designer</b> dans une équipe de développement</p>
                                <!-- FAIRE UNE MODIF ENTRE LINKS AND BOLD -->
                            </div>
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-lg-10">
                            <div class="about_banner">
                                <img src="/img/about/about_banner.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- service_area  -->
        <div class="service_area">
            <div class="outline_text white d-none d-lg-block">
                <img src="/img/bg.svg" alt="">
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-6">
                        <div class="single_service">
                            <div class="icon">
                                <img src="/img/svg_icon/1.svg" alt="">
                            </div>
                            <h3>Game & Level Design</h3>
                            <p>Logiciels utilisés : Unity, Blender/Maya, Word/Excel (Game Design), Illustrator (Level Design), Hammer (Level Editor Team Fortress 2)  </p>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6">
                        <div class="single_service">
                            <div class="icon">
                                <img src="/img/svg_icon/2.svg" alt="">
                            </div>
                            <h3>Infographie</h3>
                            <p>La création vectorielle permet de composer <br> une image simple, épurée et compréhensible. <br> Je l'utilise pour du design d'interface <br> et pour le Level Design.</p>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6">
                        <div class="single_service">
                            <div class="icon">
                                <img src="/img/svg_icon/3.svg" alt="">
                            </div>
                            <h3>Programmation</h3>
                            <p> Je sais utiliser le HTML, CSS et JS. <br> Depuis l'année 2019, je pratique régulièrement <br> la programmation en C#. Je souhaites <br> apprendre le C et C++ avec l'utilisation de Unreal Engine</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/ service_area  -->
        <div class="creative_people">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section_title text-center">
                            <h4>Les professionnels créatifs</h4>
                        </div>
                    </div>
                </div>
                <div class="border_bottom">
                    <div class="row">
                        <div class="col-lg-3 col-md-6">
                            <div class="single_people">
                                <div class="thumb">
                                    <a target="_blank" href="https://www.instagram.com/saltea.png/"><img src="/img/creative_blog/author2.png" alt="">
                                    </div>
                                    <div class="people_info">
                                        <h4>MANON POPIELARZ</h4>
                                        <p>Sound Designer, Photographe, Monteuse</p></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6">
                                <div class="single_people">
                                    <div class="thumb">
                                        <a target="_blank" href="https://www.artstation.com/steelar_bubbles"><img src="/img/creative_blog/author3.png" alt="">
                                        </div>
                                        <div class="people_info">
                                            <h4>Simon Sigal</h4>
                                            <p>FX Artist, Environnement artist</p></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-6">
                                    <div class="single_people">
                                        <div class="thumb">
                                            <a target="_blank" href="https://www.instagram.com/clementmbt_design/">
                                                <img src="/img/creative_blog/author.png" alt="">
                                            </div>
                                            <div class="people_info">
                                                <h4>Clément Maubert</h4>
                                                <p>Artiste Modélisateur et Animateur 3D</p></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-6">
                                        <div class="single_people">
                                            <div class="thumb">
                                                <img src="/img/creative_blog/author4.png" alt="">
                                            </div>
                                            <div class="people_info">
                                                <h4>Jacklyn Pusung</h4>
                                                <p>Graphic Designer, Front-End Web Design</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <!-- discuss_projects_start  -->
                    <div class="discuss_projects">
                        <div class="outline_text white project d-none d-lg-block">
                            <img src="/img/bg.svg" alt="">
                        </div>
                        <div class="container">
                            <div class="row">
                                <div class="col-xl-12">
                                    <div class="project_text text-center">
                                        <h3>Contactez-moi pour commencer à travailler ensemble</h3>
                                        <a class="boxed-btn3" href="<?= $language . "#contact" ?>">Me contacter</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- discuss_projects_end  -->
                    <!-- footer start -->
                    <footer class="footer">
                        <div class="copy-right_text">
                            <div class="container">
                                <div class="row">
                                    <div class="col-xl-12">
                                        <p class="copy_right text-center">
                                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                            Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </footer>
                    <!--/ footer end  -->
                    <!-- JS here -->
                    <script src="js/vendor/modernizr-3.5.0.min.js"></script>
                    <script src="js/vendor/jquery-1.12.4.min.js"></script>
                    <script src="js/popper.min.js"></script>
                    <script src="js/bootstrap.min.js"></script>
                    <script src="js/owl.carousel.min.js"></script>
                    <script src="js/isotope.pkgd.min.js"></script>
                    <script src="js/ajax-form.js"></script>
                    <script src="js/waypoints.min.js"></script>
                    <script src="js/jquery.counterup.min.js"></script>
                    <script src="js/imagesloaded.pkgd.min.js"></script>
                    <script src="js/scrollIt.js"></script>
                    <script src="js/jquery.scrollUp.min.js"></script>
                    <script src="js/wow.min.js"></script>
                    <script src="js/nice-select.min.js"></script>
                    <script src="js/jquery.slicknav.min.js"></script>
                    <script src="js/jquery.magnific-popup.min.js"></script>
                    <script src="js/plugins.js"></script>
                    <script src="js/gijgo.min.js"></script>
                    <!--contact js-->
                    <script src="js/contact.js"></script>
                    <script src="js/jquery.ajaxchimp.min.js"></script>
                    <script src="js/jquery.form.js"></script>
                    <script src="js/jquery.validate.min.js"></script>
                    <script src="js/mail-script.js"></script>
                    <script src="js/main.js"></script>
                </body>
            </html>