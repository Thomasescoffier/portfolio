<?php

$defaultLanguage = 'fr';
$requestedUrl = $_SERVER['REQUEST_URI'];

// Redirect to default language
if ('/' === $requestedUrl) {
    return header('Location: /' . $defaultLanguage);
}

$language = substr($requestedUrl, 0, 3);
$requestResource = substr($requestedUrl, 3);

$otherLangugage = '/fr' === $language ? 'en' : 'fr';

switch ($requestResource) {
    case '':
        include "./templates/" . $language . "/hub.php";
        break;
    
        case '/about':
        include "./templates/" . $language . "/about.php";
        break;

    case '/projects':
        include "./templates/" . $language . "/projects/all.php";
        break;

    case '/projects/emily':
        include "./templates/" . $language . "/projects/emily.php";
        break;

    case '/projects/you-manity':
        include "./templates/" . $language . "/projects/you.php";
        break;

    case '/projects/vive-le-roi':
        include "./templates/" . $language . "/projects/roi.php";
        break;

    case '/projects/dark-school':
        include "./templates/" . $language . "/projects/dark.php";
        break;

    case '/projects/organisation':
        include "./templates/" . $language . "/projects/org.php";
        break;

    case '/projects/internship-hexaverse':
        include "./templates/" . $language . "/projects/hexa.php";
        break;

    case '/projects/zappening':
        include "./templates/" . $language . "/projects/zap.php";
        break;

    case '/projects/graphic-design':
        include "./templates/" . $language . "/projects/info.php";
        break;

    case '/projects/dungeon-and-dragon':
        include "./templates/" . $language . "/projects/dnd.php";
        break;

    case '/projects/gamejams':
        include "./templates/" . $language . "/projects/jam.php";
        break;

    default:
        return header('Location: ' . $language);
        break;
}
